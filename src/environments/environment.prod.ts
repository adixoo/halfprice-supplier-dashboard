export const environment = {
  production: true,
  baseUrl: "https://wearable-hub-api.boat-lifestyle.com/v1",
  tokenKey: "authorization",
};
