// In-Built Modules
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { CanSharedModule } from 'src/@can/modules/shared.module';
import { DashboardRouteModule } from './dashboard.route';
import { RouterModule } from '@angular/router';
import { WelcomeComponent } from './components/welcome/welcome.component';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ConsumersComponent } from './components/consumers/consumers.component';
import { VendorsComponent } from './components/vendors/vendors.component';
import { InternalsComponent } from './components/internals/internals.component';
import { SupportsComponent } from './components/supports/supports.component';
import { AddUserRoleComponent } from './components/add-user-role/add-user-role.component';
import { RolesComponent } from './components/roles/roles.component';
import { CreateRolesComponent } from './components/roles/create-roles/create-roles.component';
import { EditRolesComponent } from './components/roles/edit-roles/edit-roles.component';
import { PermissionComponent } from './components/permission/permission.component';
import { CreatePermissionComponent } from './components/permission/create-permission/create-permission.component';
import { EditPermissionComponent } from './components/permission/edit-permission/edit-permission.component';
import { MapRolePermissionComponent } from './components/map-role-permission/map-role-permission.component';
import { BuildersComponent } from './components/builders/builders.component';
import { CreateBuilderComponent } from './components/builders/create-builder/create-builder.component';
import { EditBuilderComponent } from './components/builders/edit-builder/edit-builder.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { CreateProjectComponent } from './components/projects/create-project/create-project.component';
import { EditProjectComponent } from './components/projects/edit-project/edit-project.component';
import { UnitsComponent } from './components/units/units.component';
import { CreateUnitComponent } from './components/units/create-unit/create-unit.component';
import { EditUnitComponent } from './components/units/edit-unit/edit-unit.component';
import { CategoryComponent } from './components/category/category.component';
import { CreateCategoryComponent } from './components/category/create-category/create-category.component';
import { EditCategoryComponent } from './components/category/edit-category/edit-category.component';
import { SubCategoryComponent } from './components/sub-category/sub-category.component';
import { MasterProductsComponent } from './components/master-products/master-products.component';
import { ColorsComponent } from './components/colors/colors.component';
import { ProductsComponent } from './components/products/products.component';
import { PackageTypeComponent } from './components/package-type/package-type.component';
import { RoomTypeComponent } from './components/room-type/room-type.component';
import { PackagesComponent } from './components/packages/packages.component';
import { CreateColorComponent } from './components/colors/create-color/create-color.component';
import { EditColorComponent } from './components/colors/edit-color/edit-color.component';
import { CreateMasterProductComponent } from './components/master-products/create-master-product/create-master-product.component';
import { EditMasterProductComponent } from './components/master-products/edit-master-product/edit-master-product.component';
import { EditConsumerComponent } from './components/consumers/edit-consumer/edit-consumer.component';
import { ConsumerDetailsComponent } from './components/consumers/consumer-details/consumer-details.component';
import { AddressesComponent } from './components/consumers/consumer-details/addresses/addresses.component';
import { EditAddressComponent } from './components/consumers/consumer-details/addresses/edit-address/edit-address.component';
import { VerdorDetailsComponent } from './components/vendors/verdor-details/verdor-details.component';
import { SupportDetailsComponent } from './components/supports/support-details/support-details.component';
import { InternalDetailsComponent } from './components/internals/internal-details/internal-details.component';
import { MasterProductDetailsComponent } from './components/master-products/master-product-details/master-product-details.component';
import { EditProductComponent } from './components/products/edit-product/edit-product.component';
import { ProductDetailsComponent } from './components/products/product-details/product-details.component';
import { BuilderDetailsComponent } from './components/builders/builder-details/builder-details.component';
import { ProjectDetailsComponent } from './components/projects/project-details/project-details.component';
import { UnitDetailsComponent } from './components/units/unit-details/unit-details.component';
import { OrdersComponent } from './components/orders/orders.component';
import { OrderDetailsComponent } from './components/orders/order-details/order-details.component';
import { BasicInformationComponent } from './components/orders/basic-information/basic-information.component';
import { DetailViewComponent } from './components/orders/detail-view/detail-view.component';
import { UpdatesComponent } from './components/orders/updates/updates.component';
import { TransactionsComponent } from './components/orders/basic-information/transactions/transactions.component';
import { ItemsComponent } from './components/orders/basic-information/items/items.component';
import { CustomerInfoComponent } from './components/orders/basic-information/customer-info/customer-info.component';
import { ProjectInfoComponent } from './components/orders/basic-information/project-info/project-info.component';
import { OrderInfoComponent } from './components/orders/basic-information/order-info/order-info.component';
import { PackageDetailsComponent } from './components/orders/detail-view/package-details/package-details.component';
import { PreviousUpdatesComponent } from './components/orders/updates/previous-updates/previous-updates.component';
import { PostUpdatesComponent } from './components/orders/updates/post-updates/post-updates.component';
import { CreatePackageComponent } from './components/packages/create-package/create-package.component';
import { CreateProductComponent } from './components/products/create-product/create-product.component';
import { CreateRoomTypeComponent } from './components/room-type/create-room-type/create-room-type.component';
import { CreatePackageTypeComponent } from './components/package-type/create-package-type/create-package-type.component';
import { CreateSubCategoryComponent } from './components/sub-category/create-sub-category/create-sub-category.component';
import { EditSubCategoryComponent } from './components/sub-category/edit-sub-category/edit-sub-category.component';
import { EditRoomTypeComponent } from './components/room-type/edit-room-type/edit-room-type.component';
import { EditPackageTypeComponent } from './components/package-type/edit-package-type/edit-package-type.component';
import { ConfigurationsComponent } from './components/configurations/configurations.component';
import { CreateConfigurationComponent } from './components/configurations/create-configuration/create-configuration.component';
import { EditConfigurationComponent } from './components/configurations/edit-configuration/edit-configuration.component';
import { EditPackageComponent } from './components/packages/edit-package/edit-package.component';
import { RepresentativeComponent } from './components/orders/representative/representative.component';
import { LeadsComponent } from './components/leads/leads.component';
import { OrderPaymentsComponent } from './components/orders/order-payments/order-payments.component';
import { RequestPaymentComponent } from './components/orders/order-payments/request-payment/request-payment.component';
import { RecordPaymentComponent } from './components/orders/order-payments/record-payment/record-payment.component';
import { OrderTransactionsComponent } from './components/orders/order-payments/order-transactions/order-transactions.component';
import { LeadsDetailsComponent } from './components/leads/leads-details/leads-details.component';
import { CommentsComponent } from './components/leads/comments/comments.component';
import { UpdateLeadStatusComponent } from './components/leads/update-lead-status/update-lead-status.component';
import { UploadPackagesComponent } from './components/packages/upload-packages/upload-packages.component';
import { CreateProjectConfigurationComponent } from './components/configurations/create-project-configuration/create-project-configuration.component';
import { CreateConversationsComponent } from './components/leads/create-conversations/create-conversations.component';
import { ConversationsComponent } from './components/leads/conversations/conversations.component';
import { EditLeadComponent } from './components/leads/edit-lead/edit-lead.component';
import { FollowUpsComponent } from './components/leads/follow-ups/follow-ups.component';
import { CreateFollowUpsComponent } from './components/leads/follow-ups/create-follow-ups/create-follow-ups.component';
import { CreateLeadsComponent } from './components/leads/create-leads/create-leads.component';
import { UpdateFollowUpsComponent } from './components/leads/follow-ups/update-follow-ups/update-follow-ups.component';
import { LeadOrdersComponent } from './components/leads/lead-orders/lead-orders.component';
import { LeadsDetailsViewComponent } from './components/leads/leads-details/leads-details-view/leads-details-view.component';
import { AddConversationComponent } from './components/leads/create-conversations/add-conversation/add-conversation.component';
import { AddNoteComponent } from './components/leads/create-conversations/add-note/add-note.component';
import { CustomersComponent } from './components/customers/customers.component';
import { CreateInternalUsersComponent } from './components/internals/create-internal-users/create-internal-users.component';
import { CustomerDetailComponent } from './components/customers/customer-detail/customer-detail.component';
import { StatsComponent } from './components/customers/customer-detail/stats/stats.component';
import { GoalsComponent } from './components/customers/customer-detail/goals/goals.component';
import { DeviceComponent } from './components/customers/customer-detail/device/device.component';
import { WearableComponent } from './components/customers/customer-detail/wearable/wearable.component';
import { BasicDetailsComponent } from './components/customers/customer-detail/basic-details/basic-details.component';
import { BasicInfoCardComponent } from './components/customers/customer-detail/basic-info-card/basic-info-card.component';
import { ActivitiesComponent } from './components/activities/activities.component';
import { CreateActivityComponent } from './components/activities/create-activity/create-activity.component';
import { EditActivityComponent } from './components/activities/edit-activity/edit-activity.component';
import { ActivityDetailsComponent } from './components/activities/activity-details/activity-details.component';
import { SportsComponent } from './components/sports/sports.component';
import { CreateSportComponent } from './components/sports/create-sport/create-sport.component';
import { EditSportsComponent } from './components/sports/edit-sports/edit-sports.component';
import { EmergencyContactsComponent } from './components/customers/customer-detail/emergency-contacts/emergency-contacts.component';
import { WatchFacesComponent } from './components/watch-faces/watch-faces.component';
import { CreateWatchFacesComponent } from './components/watch-faces/create-watch-faces/create-watch-faces.component';
import { EditWatchFacesComponent } from './components/watch-faces/edit-watch-faces/edit-watch-faces.component';
import { WatchFacesDatailViewComponent } from './components/watch-faces/watch-faces-datail-view/watch-faces-datail-view.component';
import { MapUserRoleComponent } from './components/internals/map-user-role/map-user-role.component';
import { EditUserRoleComponent } from './components/internals/edit-user-role/edit-user-role.component';
import { EditInternalComponent } from './components/internals/edit-internal/edit-internal.component';
import { FaqComponent } from './components/faq/faq.component';
import { CreateFaqComponent } from './components/faq/create-faq/create-faq.component';
import { DetailFaqComponent } from './components/faq/detail-faq/detail-faq.component';
import { EditFaqComponent } from './components/faq/edit-faq/edit-faq.component';
import { ProductFAQComponent } from './components/products/product-faq/product-faq.component';
import { MetaInfoComponent } from './components/meta-info/meta-info.component';
import { CreateMetaInfoComponent } from './components/meta-info/create-meta-info/create-meta-info.component';
import { EditMetaInfoComponent } from './components/meta-info/edit-meta-info/edit-meta-info.component';
import { ActiveUsersListComponent } from './components/products/active-users-list/active-users-list.component';
import { CollectionsComponent } from './components/collections/collections.component';
import { CreateCollectionComponent } from './components/collections/create-collection/create-collection.component';
import { EditCollectionComponent } from './components/collections/edit-collection/edit-collection.component';
import { DetailCategoryComponent } from './components/category/detail-category/detail-category.component';
import { UploadImagesComponent } from './components/upload-images/upload-images.component';
import { UploadCatelogComponent } from './components/upload-catelog/upload-catelog.component';
import { CatelogueManagementComponent } from './components/catelogue-management/catelogue-management.component';
import { CreateCatelogueComponent } from './components/catelogue-management/create-catelogue/create-catelogue.component';

@NgModule({
    declarations: [
        DashboardComponent,
        WelcomeComponent,
        ConsumersComponent,
        VendorsComponent,
        InternalsComponent,
        SupportsComponent,
        AddUserRoleComponent,
        RolesComponent,
        CreateRolesComponent,
        EditRolesComponent,
        PermissionComponent,
        CreatePermissionComponent,
        EditPermissionComponent,
        MapRolePermissionComponent,
        BuildersComponent,
        CreateBuilderComponent,
        EditBuilderComponent,
        ProjectsComponent,
        CreateProjectComponent,
        EditProjectComponent,
        UnitsComponent,
        CreateUnitComponent,
        EditUnitComponent,
        CategoryComponent,
        CreateCategoryComponent,
        EditCategoryComponent,
        SubCategoryComponent,
        MasterProductsComponent,
        ColorsComponent,
        ProductsComponent,
        PackageTypeComponent,
        RoomTypeComponent,
        PackagesComponent,
        CreateColorComponent,
        EditColorComponent,
        CreateMasterProductComponent,
        EditMasterProductComponent,
        EditConsumerComponent,
        ConsumerDetailsComponent,
        AddressesComponent,
        EditAddressComponent,
        VerdorDetailsComponent,
        SupportDetailsComponent,
        InternalDetailsComponent,
        MasterProductDetailsComponent,
        EditProductComponent,
        ProductDetailsComponent,
        BuilderDetailsComponent,
        ProjectDetailsComponent,
        UnitDetailsComponent,
        OrdersComponent,
        OrderDetailsComponent,
        BasicInformationComponent,
        DetailViewComponent,
        UpdatesComponent,
        TransactionsComponent,
        ItemsComponent,
        CustomerInfoComponent,
        ProjectInfoComponent,
        OrderInfoComponent,
        PackageDetailsComponent,
        PreviousUpdatesComponent,
        PostUpdatesComponent,
        CreatePackageComponent,
        CreateProductComponent,
        CreateRoomTypeComponent,
        CreatePackageTypeComponent,
        CreateSubCategoryComponent,
        EditSubCategoryComponent,
        EditRoomTypeComponent,
        EditPackageTypeComponent,
        ConfigurationsComponent,
        CreateConfigurationComponent,
        EditConfigurationComponent,
        EditPackageComponent,
        RepresentativeComponent,
        LeadsComponent,
        OrderPaymentsComponent,
        RequestPaymentComponent,
        RecordPaymentComponent,
        OrderTransactionsComponent,
        LeadsDetailsComponent,
        CommentsComponent,
        UpdateLeadStatusComponent,
        UploadPackagesComponent,
        CreateProjectConfigurationComponent,
        CreateConversationsComponent,
        ConversationsComponent,
        EditLeadComponent,
        FollowUpsComponent,
        CreateFollowUpsComponent,
        CreateLeadsComponent,
        UpdateFollowUpsComponent,
        LeadOrdersComponent,
        LeadsDetailsViewComponent,
        AddConversationComponent,
        AddNoteComponent,
        CustomersComponent,
        CreateInternalUsersComponent,
        CustomerDetailComponent,
        StatsComponent,
        GoalsComponent,
        DeviceComponent,
        WearableComponent,
        BasicDetailsComponent,
        BasicInfoCardComponent,
        ActivitiesComponent,
        CreateActivityComponent,
        EditActivityComponent,
        ActivityDetailsComponent,
        SportsComponent,
        CreateSportComponent,
        EditSportsComponent,
        EmergencyContactsComponent,
        WatchFacesComponent,
        CreateWatchFacesComponent,
        EditWatchFacesComponent,
        WatchFacesDatailViewComponent,
        MapUserRoleComponent,
        EditUserRoleComponent,
        EditInternalComponent,
        FaqComponent,
        CreateFaqComponent,
        DetailFaqComponent,
        EditFaqComponent,
        ProductFAQComponent,
        MetaInfoComponent,
        CreateMetaInfoComponent,
        EditMetaInfoComponent,
        ActiveUsersListComponent,
        CollectionsComponent,
        CreateCollectionComponent,
        EditCollectionComponent,
        EditProductComponent,
        DetailCategoryComponent,
        UploadImagesComponent,
        UploadCatelogComponent,
        CatelogueManagementComponent,
        CreateCatelogueComponent
    ],
    imports: [
        CanSharedModule,
        RouterModule,
        NgxChartsModule
    ],
    exports: [
        // Routes
        DashboardRouteModule

    ],
    entryComponents: [
        CreateCollectionComponent,
        EditCollectionComponent,
        EditCategoryComponent,
        CreateSubCategoryComponent,
        EditSubCategoryComponent,
        CreateProductComponent ,
        ProductFAQComponent,
        ProductDetailsComponent,
        CreateCategoryComponent,
        CreateCatelogueComponent,
        EditProductComponent
        ]
})
export class DashboardModule {}
