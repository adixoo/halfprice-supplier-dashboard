import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-user-role',
  templateUrl: './edit-user-role.component.html',
  styleUrls: ['./edit-user-role.component.scss']
})
export class EditUserRoleComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() userId: string;
  userRoles:any[] =[]
  formData: CanForm;
  constructor(
    private apiService: CanApiService
  ) { }

 async ngOnInit() {
    // this.userRoles = await this.getUserRoles()
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      // dataSource:this.userRoles,
      formFields: [
        {
          name: 'roleIds',
          type: 'select',
          placeholder: 'Roles',
          multipleSelect: true,
          // send:'notSend',
          value:'roleIds',
          select: {
            type: 'api',
            api: {
              apiPath: '/roles',
              method: 'GET',
              params: new HttpParams()
              .append(
                "and",
                JSON.stringify([
                  { name: {'ne': 'customer'} },
                  { name: {'ne': 'Customer'} }
                ])
              ),
            },
            apiDataKey:'data',
            apiValueKey: "id",
            apiViewValueKey: "name",
          },
          required: {
            value: true,
            errorMessage: 'Roles is required.'
          },
        },
          
      ],
      getApi:{
        apiPath: `/user-role/mapped-roles`,
        method:'GET',
        params:new HttpParams().append('userId',this.userId)
      },
      apiDataKey:'data',
      submitApi: {
        apiPath: `/user-role/map-roles/${this.userId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Mapping updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

  getUserRoles(): Promise<any> {
    return new Promise((resolve) => {
      const productApi: CanApi = {
        apiPath: `/user-role`,
        method: 'GET',
        params:new HttpParams().append('userId',this.userId)
      }
      this.apiService.request(productApi)
        .subscribe((res:any) => {
          // this.checkedActivity = [...res.data['checkedActivity']]
          const roleIds = []
          for (let index = 0; index < res.data.length; index++) {
            roleIds.push(res.data[index]['roleId'])
          }
          resolve({roleIds})
        })

    })
  }

}
