import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerdorDetailsComponent } from './verdor-details.component';

describe('VerdorDetailsComponent', () => {
  let component: VerdorDetailsComponent;
  let fixture: ComponentFixture<VerdorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerdorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerdorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
