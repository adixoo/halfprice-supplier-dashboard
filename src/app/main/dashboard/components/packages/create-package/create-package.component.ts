import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-package',
  templateUrl: './create-package.component.html',
  styleUrls: ['./create-package.component.scss']
})
export class CreatePackageComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;


  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      sendAll:true,
      formFields: [
        {
          name: 'type',
          type: 'text',
          placeholder: 'Type',
          required: {
            value: true,
            errorMessage: 'Type is required.'
          }
        },
        {
          name: 'priority',
          type: 'number',
          placeholder: 'Priority',
          required: {
            value: true,
            errorMessage: 'Priority required.'
          }
        },
        {
          name: 'icon',
          type: 'image',
          placeholder: 'Upload ICON Image',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        // {
        //   name: "towerId",
        //   type: "autocomplete",
        //   placeholder: "Tower",
        //   required: {
        //     value: true,
        //     errorMessage: "Tower is required."
        //   },
        //   autoComplete: {
        //     type: "api",
        //     api: {
        //       apiPath: "/towers",
        //       method: "GET",
        //       // params: new HttpParams().append("isInternal", "true")
        //     },
        //     autocompleteParamKeys: ["name"],
        //     apiValueKey: "id",
        //     apiViewValueKey: "name"
        //   }
        // },
        {
          name: "builderId",
          type: "autocomplete",
          placeholder: "Builder",
          required: {
            value: true,
            errorMessage: "Builder is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/builders",
              method: "GET",
              // params: new HttpParams().append("isInternal", "true")
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: "projectId",
          type: "autocomplete",
          placeholder: "Project",
          required: {
            value: true,
            errorMessage: "Project is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/projects",
              method: "GET",
              // params: new HttpParams().append("isInternal", "true")
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        // {
        //   name: 'packageDetails',
        //   type: 'array',
        //   placeholder: 'Package Details',
        //   formArray: {
        //     addButton: {
        //       type: 'raised',
        //       color: 'primary',
        //       label: 'Add Package'
        //     },
        //     deleteButton: {
        //       type: 'raised',
        //       color: 'primary',
        //       label: 'Delete Package'
        //     }
        //   },
          
        //   formFields: [    
          
        //     {
        //       name: 'type',
        //       type: 'select',
        //       placeholder: 'Type',
        //       select: {
        //         type: 'api',
        //         api: {
        //           apiPath: '/room-types',
        //           method: 'GET'
        //         },
        //         apiValueKey: "name",
        //         apiViewValueKey: "name"
        //       },
        //       required: {
        //         value: true,
        //         errorMessage: 'Type is required!'
        //       }
        //     },
        //     {
        //       name: 'categories',
        //       type: 'array',
        //       placeholder: 'Categories',
        //       formArray: {
        //         addButton: {
        //           type: 'raised',
        //           color: 'primary',
        //           label: 'Add Category'
        //         },
        //         deleteButton: {
        //           type: 'raised',
        //           color: 'primary',
        //           label: 'Delete Category'
        //         }
        //       },
        //       formFields: [   
        //         {
        //           name: 'name',
        //           type: 'select',
        //           placeholder: 'Names',
        //           select: {
        //             type: 'api',
        //             api: {
        //               apiPath: '/categories',
        //               method: 'GET'
        //             },
        //             apiValueKey: "name",
        //             apiViewValueKey: "name"
        //           },
        //           required: {
        //             value: true,
        //             errorMessage: 'Name is required!'
        //           },
        //           relativeSetFields:[{
        //             key:'id',
        //             type:'different',
        //             value :'id',
        //             differentType:'dataSource'
        //           }]
              
        //         }, 
        //         {
        //           name:'id',
        //           type:'hidden',
        //           value:'id',
        //           placeholder:null
        //         },
        //         {
        //           name: 'items',
        //           type: 'array',
        //           placeholder: 'items',
        //           formArray: {
        //             addButton: {
        //               type: 'raised',
        //               color: 'primary',
        //               label: 'Add Items'
        //             },
        //             deleteButton: {
        //               type: 'raised',
        //               color: 'primary',
        //               label: 'Delete Items'
        //             }
        //           },
        //           formFields: [   
        //             {
        //               name: 'brand',
        //               type: 'select',
        //               placeholder: 'Brand',
        //               select: {
        //                 type: 'api',
        //                 api: {
        //                   apiPath: '/products',
        //                   method: 'GET'
        //                 },
        //                 apiValueKey: "brandName",
        //                 apiViewValueKey: "name"
        //               },
        //               required: {
        //                 value: true,
        //                 errorMessage: 'Brand is required!'
        //               },
        //               relativeSetFields:[{
        //                 key:'pId',
        //                 type:'different',
        //                 value :'id',
        //                 differentType:'dataSource'
        //               },
        //               {
        //                 key:'price',
        //                 type:'different',
        //                 value :'sellingPrice',
        //                 differentType:'dataSource'
        //               }]
                  
        //             }, 
        //             {
        //               name:'pId',
        //               type:'hidden',
        //               value:'id',
        //               placeholder:null,
        //               relativeSetFields:[{
        //                 key:'price',
        //                 type:'different',
        //                 value :'price',
        //                 differentType:'dataSource'
        //               }]
        //             },
        //             {
        //               name:'price',
        //               type:'hidden',
        //               value:'price',
        //               placeholder:null,
        //               // required: {
        //               //   value: true,
        //               //   errorMessage: "Price is required."
        //               // },
        //             }
        //           ]
        //         }
        //       ]
        //     }
        //   ]
        // },
      ],
      submitApi: {
        apiPath: `/packages`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Package created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
