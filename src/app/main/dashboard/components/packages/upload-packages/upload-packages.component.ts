import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanNotificationService } from 'src/@can/services/notification/notification.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanForm } from 'src/@can/types/form.type';
import { CanButton } from 'src/@can/types/shared.type';

@Component({
  selector: 'app-upload-packages',
  templateUrl: './upload-packages.component.html',
  styleUrls: ['./upload-packages.component.scss']
})
export class UploadPackagesComponent implements OnInit {

  constructor(private apiService: CanApiService,  private notificationService: CanNotificationService) { }

 
  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;
  public uploadButtonConfig : CanButton;
  public file:any;
  public multiple: false;

  ngOnInit() {
    this.uploadButtonConfig = {
      label : 'Upload',
      type : 'raised',
      color : 'primary'
    }
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      sendAll:true,
      formFields: [
        {
          name: 'file',
          type: 'document',
          placeholder: 'Upload ICON Image',
          file: {
            api: {
              apiPath: '/packages/upload/package-details',
              method: 'POST'
            },
            apiKey: 'file[0].path',
            payloadName: 'file',
            acceptType: 'csv/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
      ],
      submitApi: {
        apiPath: `/packages/upload/package-details`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Package uploading is in process!"
    }
  }

  uploadFile(){
  
   
    const submitApi:CanApi = {
      apiPath:`/packages/upload/package-details`,
      method:'POST'
    }

    this.apiService.request(submitApi,this.file).subscribe(res =>{
      if(res){
        this.notificationService.showError("Successfully submitted")
         
      }
    })
  }
  upload(event){
      // initialze form data
      const formData: FormData = new FormData();
      // Loop over Files
      for (let i = 0; i < event.target.files.length; i++) {
        // file def's
        const file = event.target.files[i];
        // add file to formdata
        formData.append('file', file, file.name);
      }
      this.file = formData;
  }
  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
