import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPackagesComponent } from './upload-packages.component';

describe('UploadPackagesComponent', () => {
  let component: UploadPackagesComponent;
  let fixture: ComponentFixture<UploadPackagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadPackagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadPackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
