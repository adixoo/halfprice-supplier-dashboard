import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiType } from 'src/@can/types/api.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreatePackageComponent } from './create-package/create-package.component';
import { EditPackageComponent } from './edit-package/edit-package.component';
import { UploadPackagesComponent } from './upload-packages/upload-packages.component';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor() { }

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Id',
            type: 'text',
            value: 'id',
          },
          {
            header: 'Type',
            type: 'text',
            value: 'type',
          },
          {
            header :'Configuration',
            type:'text',
            value:'configuration.name'
          },
          {
            header :'Project Name',
            type:'text',
            value:'project.name'
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          },
          {
            header: 'Icon',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'icon'
                }
              ]
            }
          },
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditPackageComponent,
                inputData: [
                  {
                    inputKey: 'packageId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Package',
                width: 600
              },
      permission: { type: 'single', match: { key: 'UPDATE_PACKAGE', value: true }}

            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Package',
            }
          }
        ],
        upload:{
          api:{
            apiPath:'/packages/upload/package-details',
            method:'POST',
          },
          acceptType:'csv',
          label:'Upload',
          placeholder:'Choose package details',
          payloadName:'file',
          confirm:{
            buttonText:{
              cancel:'cancel',
              confirm:'upload'
            },
            message:'Are you sure?',
            title:'Pacakge Details'
          }
        },
        filters: [
          {
            filtertype: 'api',
            placeholder: 'Search',
            type: 'text',
            key: 'id',
            searchType: 'autocomplete',
            autoComplete: {
              type: 'api',
              apiValueKey: 'id',
              apiViewValueKey: 'type',
              autocompleteParamKeys: ['type'],
              api: {
                apiPath: '/packages',
                method: 'GET'
              }
            },
          },
          {
            filtertype: 'api',
            type: 'download',
            placeholder: 'Download',
            download: {
              downloadType: "api",
              extension: ".xlsx",
              api: {
                apiPath: "/packages",
                apiType: ApiType.Loopback,
                responseType: "blob",
                method: "GET",
                params: new HttpParams()  
                  .append('exportExcel', JSON.stringify({ headerDisplayType: 'propercase' , "exportAll":true})),
              },
            }
          }
        ],
        api: {
          apiPath: '/packages',
          method: 'GET',
          params: new HttpParams()
          .append('include',JSON.stringify({all : true}))
        },
        countApi: {
          apiPath: '/packages/count',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100, 200]
        },
        header: 'Packages'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreatePackageComponent,
        inputData: [],
        width: 600,
        header: 'Add Package '
      },
      permission: { type: 'single', match: { key: 'CREATE_PACKAGE', value: true }}
              
    }
  }

}
