import { EventEmitter, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-upload-images',
  templateUrl: './upload-images.component.html',
  styleUrls: ['./upload-images.component.scss']
})
export class UploadImagesComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;

  public tableConfig: CanTable;
  public uploadedImage:any

  ngOnInit() {
    this.formData = {
      type: "create",
      discriminator: "formConfig",
      columnPerRow: 1,
      sendAll: true,
      formFields: [
        {
          name: "thumbImages",
          type: "image",
          placeholder: "Upload Images",
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST",
            },
            apiKey: "files.paths",
            payloadName: "files",
            acceptType: "image/*",
            multipleSelect: true,
          },
          suffixIcon: { name: "attach_file" },
        },
      ],
      submitApi: {
        apiPath: `/files/upload`,
        method: "POST",
      },
      formButton: {
        type: "raised",
        color: "primary",
        label: "Save",
      },
      submitSuccessMessage: "Uploaded successfully!",
    };
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

  fileUploaded(res){
    this.uploadedImage = res.files
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        dataSource : this.uploadedImage,
        displayedColumns: [
          {
            header: 'Image',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'path'
                }
              ]
            }
          },
         
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Url',
            type: 'text',
            value: 'path',
            wordWrap: true
          }
        ],
       
        api: {
          apiPath: '/orders',
          method: 'GET',
         },
        pagination: {
          pageSizeOptions: [100,200]
        },
        header: 'Images'
      }
  }

}
