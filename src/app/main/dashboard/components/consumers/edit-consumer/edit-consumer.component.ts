import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-consumer',
  templateUrl: './edit-consumer.component.html',
  styleUrls: ['./edit-consumer.component.scss']
})
export class EditConsumerComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() userId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [        
        {
          name: 'firstName',
          type: 'text',
          placeholder: 'First Name',
          value:'firstName',
          required: {
            value: true,
            errorMessage: 'First Name is required.'
          }
        },
        {
          name: 'lastName',
          type: 'text',
          placeholder: 'Last Name',
          value:'lastName',
          required: {
            value: true,
            errorMessage: 'Last Name is required.'
          }
        },
        {
          name: 'middleName',
          type: 'text',
          placeholder: 'Middle Name',
          value:'middleName'
        },
        {
          name: 'businessName',
          type: 'text',
          placeholder: 'Business Name',
          value:'businessName'
        },
        {
          name: 'email',
          type: 'text',
          placeholder: 'Email',
          value:'email',
          required: {
            value: true,
            errorMessage: 'Email is required.'
          }
        },
        {
          name: 'mobile',
          type: 'text',
          placeholder: 'Phone',
          value:'mobile',
          required: {
            value: true,
            errorMessage: 'Mobile is required.'
          }
        },
        {
          name: 'type',
          type: 'select',
          placeholder: 'Type',
          value:'type',
          values: [
            { value: 'consumer', viewValue: 'Consumer' },
            { value: 'vendor', viewValue: 'Vendor' },
            { value: 'internal', viewValue: 'Internal' },
            { value: 'support', viewValue: 'Support' },

          ]
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ]
        }
      ],
      getApi:{
        apiPath: `/users/${this.userId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/users/${this.userId}`,
        method: 'PATCH'
      },
      // header: 'Edit Consumer',
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Consumer updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
