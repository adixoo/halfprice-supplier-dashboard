import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { EditConsumerComponent } from '../edit-consumer/edit-consumer.component'
import { AddressesComponent } from './addresses/addresses.component';

@Component({
  selector: 'app-consumer-details',
  templateUrl: './consumer-details.component.html',
  styleUrls: ['./consumer-details.component.scss']
})
export class ConsumerDetailsComponent implements OnInit {

  public consumerId:any;

  public detailViewData: CanDetailView;
  public tabViewConfig: CanTabView;
  
  constructor(private activedRoute: ActivatedRoute) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => this.consumerId = parseInt(paramMap.get('id')));
  }

  ngOnInit() {
    // Detail View Config Init
    this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 1,
      labelPosition: 'top',

      action: [{
        action: {
          actionType: 'modal',
          modal: {
            component: EditConsumerComponent,
            inputData: [
              {
                inputKey: 'userId',
                type: 'key',
                key: 'id'
              }
            ],
            // header: 'Edit Consumer',
            width: 600
          }
        },
        icon: {
          name: 'edit',
          tooltip: 'Edit'
        },
      }],
      displayedFields: [
        {
          type: 'group',
          group: {
            columnPerRow: 4,
            labelPosition: 'top',
            displayedFields: [
              {
                header: 'First Name',
                type: 'text',
                value: 'firstName'
              },
              {
                header: 'Middle Name',
                type: 'text',
                value: 'middleName'
              },
              {
                header: 'Last Name',
                type: 'text',
                value: 'lastName'
              },
              {
                header: 'Name',
                type: 'text',
                value: 'firstName middleName lastName',
                keySeparators:[' ']
              },
              {
                header: 'Email',
                type: 'text',
                value: 'email',
              },
              // {
              //   header:'Business Name',
              //   type:'text',
              //   value:'businessName'
              // },
              {
                header: 'Phone',
                type: 'text',
                value: 'mobile',
              },
              {
                header: 'Type',
                type: 'text',
                value: 'type',
              },
              {
                header: 'Status',
                type: 'text',
                value: 'status',
              }
            ],
          }
        },
        // {
        //   header: 'Residence Information',
        //   type: 'group',
        //   displayCondition: { type: 'single', match: { operator: 'notEmpty', key: 'residenceInfo' } },
        //   group: {
        //     columnPerRow: 4,
        //     labelPosition: 'top',
        //     displayedFields: [
        //       {
        //         header: 'Bed',
        //         type: 'text',
        //         value: 'residenceInfo.bed',
        //       },
        //       {
        //         header: 'Property',
        //         type: 'text',
        //         value: 'residenceInfo.property',
        //       },
        //       {
        //         header: 'Tower',
        //         type: 'text',
        //         value: 'residenceInfo.tower',
        //       },
        //       {
        //         header: 'Floor',
        //         type: 'text',
        //         value: 'residenceInfo.floor',
        //       },
        //       {
        //         header: 'Unit',
        //         type: 'text',
        //         value: 'residenceInfo.unit',
        //       },
        //       {
        //         header: 'Occupied From',
        //         type: 'date',
        //         value: 'residenceInfo.occupiedFrom',
        //         dateDisplayType: 'dd-MMM-yyyy'
        //       },
        //       {
        //         header: 'Occupied Until',
        //         type: 'date',
        //         value: 'residenceInfo.occupiedUntill',
        //         dateDisplayType: 'dd-MMM-yyyy'
        //       },
        //       {
        //         header: 'Address',
        //         type: 'text',
        //         value: 'residenceInfo.address, residenceInfo.city, residenceInfo.state, residenceInfo.country, residenceInfo.pinCode',
        //         keySeparators: [', ']
        //       }
        //     ],
        //   },
        // },
      ],
      header: 'Consumer Information',
      api: {
        apiPath: `/users/${this.consumerId}`,
        method: 'GET'
      }
    }
  }

  onGetData(data) {
    this.tabViewConfig = {
      lazyLoading: true,
      tabs: []
    };
    this.tabViewConfig.tabs.push({
      component: AddressesComponent,
      label: 'Adderess',
      inputData: [{
        key: 'userId',
        value: this.consumerId
      }]
    })
   

  }

}
