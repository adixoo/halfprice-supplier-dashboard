import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { ConsumerDetailsComponent } from './consumer-details/consumer-details.component';
import { EditConsumerComponent } from './edit-consumer/edit-consumer.component'

@Component({
  selector: 'app-consumer',
  templateUrl: './consumers.component.html',
  styleUrls: ['./consumers.component.scss']
})
export class ConsumersComponent implements OnInit {

  constructor() { }
  public tableConfig: CanTable;

  ngOnInit() {
 
    // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Name',
          type: 'text',
          value: 'firstName middleName lastName',
          keySeparators:[' ']
        },
        {
          header: 'Email',
          type: 'text',
          value: 'email',
        },
        {
          header:'Business Name',
          type:'text',
          value:'businessName'
        },
        {
          header: 'Phone',
          type: 'text',
          value: 'mobile',
        },
        {
          header: 'Type',
          type: 'text',
          value: 'type',
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        }
      ],
      filters: [
        {
          filtertype: 'api',
          placeholder: 'Search',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'name',
            autocompleteParamKeys: ['firstName', 'lastName', 'email', 'mobile'],
            api: {
              apiPath: '/users',
              method: 'GET',
              params : new HttpParams().set('type', 'consumer')
            }
          },
        },

      ],
      fieldActions: [
  
        {
          action: {
            actionType: 'modal',
            modal: {
              component: EditConsumerComponent,
              inputData: [
                {
                  inputKey: 'userId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Edit Consumer',
              width: 600
            }
          },
          icon: {
            name: 'edit',
            tooltip: 'Edit Category',
          }
        },
        {
          action: {
            actionType: 'link',
            link: {
              url: '/dashboard/consumer/${id}',
              target: 'self',
              type: 'url'
            }
          },
          icon: {
            type: CanIconType.Flaticon,
            name: 'flaticon-eye',
            tooltip: 'View'
          }
        },
        // {
        //   action: {
        //     actionType: 'link',

        //     link: {
        //       url: '/dashboard/sub-category/${id}',
        //       target: 'self',
        //       type: 'url'
        //     }
        //     // modal: {
        //     //   component: ServiceSubTableComponent,
        //     //   inputData: [
        //     //     {
        //     //       inputKey: 'serviceId',
        //     //       type: 'key',
        //     //       key: 'id'
        //     //     }
        //     //   ],
        //     //   header: 'Service Sub Categories',
        //     //   width: 1000
        //     // },

        //   },
        //   icon: {
        //     type: CanIconType.Flaticon,
        //     name: 'flaticon-eye',
        //     tooltip: 'Service Sub Categories',
        //   }

        // }
      ],
      api: {
        apiPath: '/users',
        method: 'GET',
        params : new HttpParams().set('type', 'consumer')
      },
      countApi: {
        apiPath: '/users/count',
        method: 'GET',
        params : new HttpParams().set('type', 'consumer')

      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100 , 200]
      },
      header: 'Consumers'
    }
  }

}
