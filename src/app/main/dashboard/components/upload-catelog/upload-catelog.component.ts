import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-upload-catelog',
  templateUrl: './upload-catelog.component.html',
  styleUrls: ['./upload-catelog.component.scss']
})
export class UploadCatelogComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;

  public tableConfig: CanTable;

  ngOnInit() {
    this.formData = {
      type: "create",
      discriminator: "formConfig",
      columnPerRow: 1,
      sendAll: true,
      formFields: [
        {
          name: "thumbImages",
          type: "document",
          placeholder: "Upload Catelog",
          file: {
            api: {
              apiPath: "/products/upload",
              method: "POST",
            },
            apiKey: "files.paths",
            payloadName: "files",
            acceptType: "csv/*",
            multipleSelect: false,
          },
          suffixIcon: { name: "attach_file" },
        },
      ],
      submitApi: {
        apiPath: `/files/upload`,
        method: "POST",
      },
      formButton: {
        type: "raised",
        color: "primary",
        label: "Save",
      },
      submitSuccessMessage: "Uploaded successfully!",
    };
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
