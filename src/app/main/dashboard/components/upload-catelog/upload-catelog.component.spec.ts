import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadCatelogComponent } from './upload-catelog.component';

describe('UploadCatelogComponent', () => {
  let component: UploadCatelogComponent;
  let fixture: ComponentFixture<UploadCatelogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadCatelogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadCatelogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
