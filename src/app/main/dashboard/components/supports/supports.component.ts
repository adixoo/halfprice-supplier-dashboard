import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { AddUserRoleComponent } from '../add-user-role/add-user-role.component';
import { EditConsumerComponent } from '../consumers/edit-consumer/edit-consumer.component';

@Component({
  selector: 'app-support',
  templateUrl: './supports.component.html',
  styleUrls: ['./supports.component.scss']
})
export class SupportsComponent implements OnInit {

 
  constructor() { }
  public tableConfig: CanTable;

  ngOnInit() {
  
    // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Name',
          type: 'text',
          value: 'firstName middleName lastName',
          keySeparators:[' ']
        },
        {
          header: 'Email',
          type: 'text',
          value: 'email',
        },
        {
          header:'Business Name',
          type:'text',
          value:'businessName'
        },
        {
          header: 'Phone',
          type: 'text',
          value: 'mobile',
        },
        {
          header: 'Type',
          type: 'text',
          value: 'type',
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        }
      ],
      fieldActions: [

        {
          action: {
            actionType: 'modal',
            modal: {
              component: AddUserRoleComponent,
              inputData: [
                {
                  inputKey: 'userId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Add User Roles',
              width: 600
            }
          },
          icon: {
            name: 'add role',
            tooltip: 'Add User Role',
          }
        },
        {
          action: {
            actionType: 'modal',
            modal: {
              component: EditConsumerComponent,
              inputData: [
                {
                  inputKey: 'userId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Edit Support User',
              width: 600
            }
          },
          icon: {
            name: 'edit',
            tooltip: 'Edit Support User',
          }
        },
        {
          action: {
            actionType: 'link',
            link: {
              url: '/dashboard/support/${id}',
              target: 'self',
              type: 'url'
            }
          },
          icon: {
            type: CanIconType.Flaticon,
            name: 'flaticon-eye',
            tooltip: 'View'
          }
        },
      ],
      filters: [
        {
          filtertype: 'api',
          placeholder: 'Search',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'name',
            autocompleteParamKeys: ['firstName', 'lastName', 'email', 'mobile'],
            api: {
              apiPath: '/users',
              method: 'GET',
              params : new HttpParams().set('type', 'support')
            }
          },
        },
      ],
      api: {
        apiPath: '/users',
        method: 'GET',
        params : new HttpParams().set('type', 'support')
      },
      countApi: {
        apiPath: '/users/count',
        method: 'GET',
        params : new HttpParams().set('type', 'support')

      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100, 200]
      },
      header: 'Supports'
    }
  }
}
