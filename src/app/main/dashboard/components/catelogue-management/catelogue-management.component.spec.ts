import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatelogueManagementComponent } from './catelogue-management.component';

describe('CatelogueManagementComponent', () => {
  let component: CatelogueManagementComponent;
  let fixture: ComponentFixture<CatelogueManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatelogueManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatelogueManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
