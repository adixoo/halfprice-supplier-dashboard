import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanAuthService } from 'src/@can/services/auth/auth.service';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-catelogue',
  templateUrl: './create-catelogue.component.html',
  styleUrls: ['./create-catelogue.component.scss']
})
export class CreateCatelogueComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;
  public businessId:any;


  constructor( private authService : CanAuthService) {
    this.businessId = this.authService.getBusinessId()
  }

  async ngOnInit() {
    this.formData = {
      type: "create",
      discriminator: "formConfig",
      sendAll: true,
      columnPerRow: 1,
      formFields: [
        {
          name: "name",
          type: "text",
          placeholder: "Name"
        }, 
        {
          name: "collectionId",
          type: "autocomplete",
          send:'notSend',
          placeholder: "Collections",
          required: {
            value: true,
            errorMessage: "Collections is required."
          },
          relatedTo:['categoryId'],
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/collections",
              method: "GET"
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: "categoryId",
          type: "autocomplete",
          send:'notSend',
          placeholder: "Category",
          required: {
            value: true,
            errorMessage: "Category is required."
          },
          relatedTo:['subCategoryId'],
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/categories",
              method: "GET"
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name",
          },
                
        },
        // {
        //   name: 'categoryId',
        //   type: 'select',
        //   placeholder: 'Category',
        //   relatedTo:['subCategoryId'],
        //   select: {
        //     type: 'api',
        //     api: {
        //       apiPath: '/categories',
        //       method: 'GET',
        //     },
        //     apiViewValueKey: 'name',
        //     apiValueKey: 'id',
        //     relativeApiInfo: {
        //       queryParams: [{ key: 'collectionId', value: 'collectionId' }]
        //     }
        //   }
        // },
        {
          name: 'subCategoryId',
          type: 'select',
          placeholder: 'Sub Category',
          select: {
            type: 'api',
            api: {
              apiPath: '/sub-categories',
              method: 'GET',
            },
            apiViewValueKey: 'name',
            apiValueKey: 'id',
            relativeApiInfo: {
              queryParams: [{ key: 'categoryId', value: 'categoryId' }]
            }
          }
        },
        {
          name: 'csvUrl',
          type: 'document',
          placeholder: 'Upload Catelogue',
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST",
              params: new HttpParams().append('channel','cloudinary')
            },
            apiKey: 'files[0]',
            payloadName: 'files',
            acceptType: 'csv/*',
            limit: 1,
            multipleSelect: false,
            
          },
          suffixIcon: { name: 'arrow-upward' }
        },
        {
          name:"businessId",
          type: "hidden",
          placeholder:null,
          defaultValue:this.businessId
        }
        
      ],
      submitApi: {
        apiPath: "/catelouges",
        method: "POST",
      },

      formButton: {
        type: "raised",
        color: "primary",
        label: "Save"
      },
      submitSuccessMessage: "FAQ submited successfully!"
    };
  }
  formSubmitted(event) {
    this.outputEvent.emit(event);
  }

}
