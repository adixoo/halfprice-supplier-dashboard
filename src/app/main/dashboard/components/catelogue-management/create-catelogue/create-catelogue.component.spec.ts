import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCatelogueComponent } from './create-catelogue.component';

describe('CreateCatelogueComponent', () => {
  let component: CreateCatelogueComponent;
  let fixture: ComponentFixture<CreateCatelogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCatelogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCatelogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
