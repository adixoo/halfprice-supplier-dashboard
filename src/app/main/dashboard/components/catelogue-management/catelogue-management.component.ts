import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateCatelogueComponent } from './create-catelogue/create-catelogue.component';

@Component({
  selector: 'app-catelogue-management',
  templateUrl: './catelogue-management.component.html',
  styleUrls: ['./catelogue-management.component.scss']
})
export class CatelogueManagementComponent implements OnInit {

  public tableData: CanTable;
  public fabButtonConfig: CanFabButton;
  constructor() { }

  ngOnInit() {

    this.tableData = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Name',
          type: 'text',
          value: 'name'
        },
        {
            header: 'CSV',
            type: 'document',
            documents: {
            openType: "link",
            documentItems: [
              {
                type: "api",
                value: "csv"
              }
            ],
            linkType: "self",
            showAll: true
            }
          },
        {
          header: 'Status',
          type: 'enum_icon',
          value: 'status',
          enumIcons: [
            { value: 'active',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Active', color:  '#10d817'} },
            { value: 'inactive',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Inactive', color:  '#ee3f37'} },

          ]
        },
      ],
      filters: [
        // {
        //   filtertype: "api",
        //   placeholder: "Status",
        //   type: "dropdown",
        //   key: "status",
        //   value: [
        //     { value: "active", viewValue: "Active" },
        //     { value: "inactive", viewValue: "Inactive" },
        //   ],
        // }
      ],
      api: {
        apiPath: '/catelouges',
        // apiType: ApiType.Loopback,
        method: 'GET',
        params : new HttpParams()
        .append('include', JSON.stringify([{  all: true  }]))
        .append('isDemo', 'true')
      },
      countApi:{
        apiPath:'/catelouges/count',
        method:'GET'
      },
      apiDataKey: "data",
  
      countApiDataKey: 'data.count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: 'Catelogues',
    }
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Add Form'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateCatelogueComponent,
        inputData: [],
        width: 600,
        header: 'Add Catelogue'
      },
  
    }
  }


}
