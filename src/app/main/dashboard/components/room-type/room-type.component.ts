import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateRoomTypeComponent } from './create-room-type/create-room-type.component';
import { EditRoomTypeComponent } from './edit-room-type/edit-room-type.component';

@Component({
  selector: 'app-room-type',
  templateUrl: './room-type.component.html',
  styleUrls: ['./room-type.component.scss']
})
export class RoomTypeComponent implements OnInit {

 
  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor() { }

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Icon',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'icon'
                }
              ]
            }
          },
          {
            header: 'Image',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'image'
                }
              ]
            }
          },
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditRoomTypeComponent,
                inputData: [
                  {
                    inputKey: 'roomTypeId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Room Type',
                width: 600
              },
      permission: { type: 'single', match: { key: 'UPDATE_ROOM-TYPE', value: true }}

            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Room Type',
            }
          }
        ],
        filters: [
          // {
          //   filtertype: 'local',
          //   placeholder: 'Search',
          //   keys: ['name']
          // }
          {
            filtertype: 'api',
            placeholder: 'Search',
            type: 'text',
            key: 'id',
            searchType: 'autocomplete',
            autoComplete: {
              type: 'api',
              apiValueKey: 'id',
              apiViewValueKey: 'name',
              autocompleteParamKeys: ['name'],
              api: {
                apiPath: '/room-types',
                method: 'GET'
              }
            },
          },
        ],
        api: {
          apiPath: '/room-types',
          method: 'GET'
        },
        countApi: {
          apiPath: '/room-types/count',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100, 200]
        },
        header: 'Room Types'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateRoomTypeComponent,
        inputData: [],
        width: 600,
        header: 'Add Room Type'
      },
      permission: { type: 'single', match: { key: 'CREATE_ROOM-TYPE', value: true }}

    }
  }

}
