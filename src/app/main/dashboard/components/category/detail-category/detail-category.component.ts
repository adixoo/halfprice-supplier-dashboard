import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanDetailView } from 'src/@can/types/detail-view.type';

@Component({
  selector: 'app-detail-category',
  templateUrl: './detail-category.component.html',
  styleUrls: ['./detail-category.component.scss']
})
export class DetailCategoryComponent implements OnInit {

  public detailViewData: CanDetailView;
  @Input() categoryId: any;
  dataSource : any
   
  constructor(private activedRoute: ActivatedRoute) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => this.categoryId = parseInt(paramMap.get('id')));
  }
  ngOnInit() {
     // Detail View Config Init
     this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 3,
      // dataSource : this.dataSource,
      labelPosition: 'inline',
      displayedFields: [
        {
          header: 'Name',
          type: 'text',
          value: 'name',
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        },
        // {
        //   header: 'Banner',
        //   type: 'image',
        //   images: {
        //     showAll: true,
        //     openType: 'modal',
        //     imageItems: [
        //       {
        //         type: 'api',
        //         isArray: true,
        //         alt: 'Preview',
        //         value: 'banners'
        //       }
        //     ]
        //   }
        // },
        // {
        //   header: 'Icon',
        //   type: 'image',
        //   images: {
        //     showAll: true,
        //     openType: 'modal',
        //     imageItems: [
        //       {
        //         type: 'api',
        //         isArray: false,
        //         alt: 'Preview',
        //         value: 'icon'
        //       }
        //     ]
        //   }
        // },
        // {
        //   header:'Logo',
        //   type:'image',
        //   value:'logo'
        // }
      ],
      api: {
        apiPath: `/categories/${this.categoryId}`,
        method: 'GET'
      },
     
      // header: "Master Products Details"
    }
    
  }

}
