import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {

 
  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;


  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'description',
          type: 'text',
          placeholder: 'Description',
          required: {
            value: true,
            errorMessage: 'Descriptionis required.'
          }
        },
        {
          name: "collectionId",
          type: "autocomplete",
          placeholder: "Collection",
          required: {
            value: true,
            errorMessage: "Collection is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/collections",
              method: "GET"
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        // {
        //   name: 'banners',
        //   type: 'image',
        //   placeholder: 'Upload Banner Image',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'image/*',
        //     limit: 2,
        //     multipleSelect: true,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
        // {
        //   name: 'icon',
        //   type: 'image',
        //   placeholder: 'Upload ICON Image',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'image/*',
        //     limit: 1,
        //     multipleSelect: false,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
        

        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
        // {
        //   name: 'name',
        //   type: 'text',
        //   placeholder: 'Name',
        //   required: {
        //     value: true,
        //     errorMessage: 'Name is required.'
        //   }
        // },
        // {
        //   name: 'status',
        //   type: 'hidden',
        //   placeholder: 'Status',
        //   defaultValue:'active'
        // }
      ],
      submitApi: {
        apiPath: `/categories`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Category created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
