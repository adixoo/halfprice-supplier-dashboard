import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() categoryId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'description',
          type: 'text',
          placeholder: 'Description',
          value:'description',
          required: {
            value: true,
            errorMessage: 'Descriptionis required.'
          }
        },
       
        {
          name: "collectionId",
          type: "autocomplete",
          value: "collectionId",
          placeholder: "Collection",
          required: {
            value: true,
            errorMessage: "Collection is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/collections",
              method: "GET"
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        

        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
        // {
        //   name: 'name',
        //   type: 'text',
        //   placeholder: 'Name',
        //   required: {
        //     value: true,
        //     errorMessage: 'Name is required.'
        //   }
        // },
        // {
        //   name: 'status',
        //   type: 'hidden',
        //   placeholder: 'Status',
        //   defaultValue:'active'
        // }
      ],
      getApi:{
        apiPath: `/categories/${this.categoryId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/categories/${this.categoryId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Color updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
