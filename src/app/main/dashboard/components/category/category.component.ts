import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { SubCategoryComponent } from '../sub-category/sub-category.component';
import { CreateCategoryComponent } from './create-category/create-category.component';
import  { EditCategoryComponent } from './edit-category/edit-category.component';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

 
  constructor() { }
  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          },
          // {
          //   header: 'Banner',
          //   type: 'image',
          //   images: {
          //     showAll: true,
          //     openType: 'modal',
          //     imageItems: [
          //       {
          //         type: 'api',
          //         isArray: true,
          //         alt: 'Preview',
          //         value: 'banners'
          //       }
          //     ]
          //   }
          // },
          // {
          //   header: 'Icon',
          //   type: 'image',
          //   images: {
          //     showAll: true,
          //     openType: 'modal',
          //     imageItems: [
          //       {
          //         type: 'api',
          //         isArray: false,
          //         alt: 'Preview',
          //         value: 'icon'
          //       }
          //     ]
          //   }
          // },
          // {
          //   header:'Logo',
          //   type:'image',
          //   value:'logo'
          // }
        ],
        // fieldActions: [
  
        //   {
        //     action: {
        //       actionType: 'modal',
        //       modal: {
        //         component: EditCategoryComponent,
        //         inputData: [
        //           {
        //             inputKey: 'categoryId',
        //             type: 'key',
        //             key: 'id'
        //           }
        //         ],
        //         header: 'Edit Category',
        //         width: 600
        //       },
     
        //     },
        //     icon: {
        //       name: 'edit',
        //       tooltip: 'Edit Category',
        //     }
        //   },
        //   {
        //     action: {
        //       actionType: 'link',

        //       link: {
        //         url: '/dashboard/category/${id}',
        //         target: 'self',
        //         type: 'url'
        //       }
        //       // modal: {
        //       //   component: ServiceSubTableComponent,
        //       //   inputData: [
        //       //     {
        //       //       inputKey: 'serviceId',
        //       //       type: 'key',
        //       //       key: 'id'
        //       //     }
        //       //   ],
        //       //   header: 'Service Sub Categories',
        //       //   width: 1000
        //       // },
  
        //     },
        //     icon: {
        //       type: CanIconType.Flaticon,
        //       name: 'flaticon-eye',
        //       tooltip: 'Categories',
        //     }

        //   }
        // ],
        filters: [
          {
            filtertype: 'api',
            placeholder: 'Search',
            type: 'text',
            key: 'id',
            searchType: 'autocomplete',
            autoComplete: {
              type: 'api',
              apiValueKey: 'id',
              apiViewValueKey: 'name',
              autocompleteParamKeys: ['name'],
              api: {
                apiPath: '/categories',
                method: 'GET'
              }
            },
          },
        ],
        api: {
          apiPath: '/categories',
          method: 'GET',
          params: new HttpParams().append('include',JSON.stringify({'all':true}))
        },
        countApi: {
          apiPath: '/categories/count',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100 , 200]
        },
        header: 'Categories'
      }

    //   // Fab Button Config
    // this.fabButtonConfig = {
    //   icon: {
    //     name: 'add',
    //     tooltip: 'Create New'
    //   },
    //   type: 'modal',
    //   position: CanFabButtonPosition.BottomRight,
    //   modal: {
    //     component: CreateCategoryComponent,
    //     inputData: [],
    //     width: 600,
    //     header: 'Add Category'
    //   },

    // }
  }

}
