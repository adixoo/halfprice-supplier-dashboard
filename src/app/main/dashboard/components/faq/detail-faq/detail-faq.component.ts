import { Component, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';

@Component({
  selector: 'app-detail-faq',
  templateUrl: './detail-faq.component.html',
  styleUrls: ['./detail-faq.component.scss']
})
export class DetailFaqComponent implements OnInit {

  constructor() { }
  private faqId: any;
  public detailViewData: CanDetailView;
  dataSource : any
  

  ngOnInit() {
       // Detail View Config Init
       this.detailViewData = {
        discriminator: 'detailViewConfig',
        columnPerRow: 1,
        dataSource:this.dataSource,
        labelPosition: 'inline',
        displayedFields: [
          {
            header: 'Title',
            type: 'text',
            value: 'title',
          },
          {
            header: 'Images',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'images'
                }
              ]
            }
          },
          {
            header : 'Created At',
            type : 'date',
            value : 'createdAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          },
          {
            header : 'Updated At',
            type : 'date',
            value : 'updatedAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          },
          {
            header : 'Product',
            type : 'text',
            value : 'product.name'
          },
          {
            header:'Description',
            type:'text',
            value:'description'
          },
        ],
   
        // header: "Master Products Details"
      }
  }

}
