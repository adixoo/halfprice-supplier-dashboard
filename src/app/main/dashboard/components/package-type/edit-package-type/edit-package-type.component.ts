import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-package-type',
  templateUrl: './edit-package-type.component.html',
  styleUrls: ['./edit-package-type.component.scss']
})
export class EditPackageTypeComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() packageTypeId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'image',
          type: 'image',
          placeholder: 'Upload Image',
          value:'image',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      getApi:{
        apiPath: `/package-types/${this.packageTypeId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/package-types/${this.packageTypeId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Package Type updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
