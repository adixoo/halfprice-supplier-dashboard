import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPackageTypeComponent } from './edit-package-type.component';

describe('EditPackageTypeComponent', () => {
  let component: EditPackageTypeComponent;
  let fixture: ComponentFixture<EditPackageTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPackageTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPackageTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
