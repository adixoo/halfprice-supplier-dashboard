import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-master-product',
  templateUrl: './create-master-product.component.html',
  styleUrls: ['./create-master-product.component.scss']
})
export class CreateMasterProductComponent implements OnInit {

 
  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;


  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'internalSku',
          type: 'text',
          placeholder: 'Internal Sku',
          required: {
            value: true,
            errorMessage: 'Internal Sku is required.'
          }
        },
        {
          name: 'externalSku',
          type: 'text',
          placeholder: 'External Sku',
          required: {
            value: true,
            errorMessage: 'External Sku is required.'
          }
        },
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'brandName',
          type: 'text',
          placeholder: 'Brand Name',
          required: {
            value: true,
            errorMessage: 'Brand Name is required.'
          }
        },
        {
          name: "categoryId",
          type: "autocomplete",
          placeholder: "Category",
          required: {
            value: true,
            errorMessage: "Category is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/categories",
              method: "GET",
              // params: new HttpParams().append("isInternal", "true")
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        {
          name: 'variantCount',
          type: 'number',
          placeholder: 'Variant Count',
          required: {
            value: true,
            errorMessage: 'Variant Count is required.'
          }
        },
        {
          name: 'availableStock',
          type: 'number',
          placeholder: 'Available Stock',
          required: {
            value: true,
            errorMessage: 'Available Stock is required.'
          }
        },
        
      ],
      submitApi: {
        apiPath: `/master-products`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Master product created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
