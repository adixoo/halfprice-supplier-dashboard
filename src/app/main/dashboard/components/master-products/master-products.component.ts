import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiType } from 'src/@can/types/api.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateMasterProductComponent } from './create-master-product/create-master-product.component'
import { EditMasterProductComponent } from './edit-master-product/edit-master-product.component'
import { MasterProductDetailsComponent } from './master-product-details/master-product-details.component';

@Component({
  selector: 'app-master-products',
  templateUrl: './master-products.component.html',
  styleUrls: ['./master-products.component.scss']
})
export class MasterProductsComponent implements OnInit {

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor() { }

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Internal Sku',
            type: 'text',
            value: 'internalSku',
          },
          {
            header: 'External Sku',
            type: 'text',
            value: 'externalSku',
          },
          {
            header : 'name',
            type : 'text',
            value:'name'
          },
          {
            header : 'Brand Name',
            type :'text',
            value :'brandName'
          },
          {
            header:'Status',
            type : 'text',
            value: 'status'
          },
          {
            header :'Variant Count',
            type : 'text',
            value : 'variantCount'
          },
          {
            header : 'Available Stock',
            type : 'text',
            value : 'availableStock'
          },
          {
            header : 'Created At',
            type : 'date',
            value : 'createdAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          },
          {
            header : 'Updated At',
            type : 'date',
            value : 'updatedAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          }
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditMasterProductComponent,
                inputData: [
                  {
                    inputKey: 'masterProductId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Master Product',
                width: 600
              },
            permission: { type: 'single', match: { key: 'UPDATE_PROJECTS', value: true }}
              
            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Master Product',
            }
          },
          {
            // action: {
            //   actionType: 'modal',
            //   modal: {
            //     component: MasterProductDetailsComponent,
            //     inputData: [
            //       {
            //         inputKey: 'masterProductId',
            //         type: 'key',
            //         key: 'id'
            //       }
            //     ],
            //     header: 'Master Product',
            //     width: 600
            //   }
            // },
            action: {
              actionType: 'link',
              link: {
                url: '/dashboard/product/${id}',
                target: 'self',
                type: 'url'
              }
            },
            icon: {
              type: CanIconType.Flaticon,
              name: 'flaticon-eye',
              tooltip: 'View'
            }
          }
        ],
        filters: [
          // {
          //   filtertype: 'local',
          //   placeholder: 'Search',
          //   keys: ['name','internalSku','externalSku']
          // },
          {
            filtertype: 'api',
            placeholder: 'Search',
            type: 'text',
            key: 'id',
            searchType: 'autocomplete',
            autoComplete: {
              type: 'api',
              apiValueKey: 'id',
              apiViewValueKey: 'name',
              autocompleteParamKeys: ['name','internalSku','externalSku'],
              api: {
                apiPath: '/master-products',
                method: 'GET'
              }
            },
          },
          {
            filtertype: "api",
            placeholder: "Status",
            type: "dropdown",
            key: "status",
            value: [
              { value: "active", viewValue: "Active" },
              { value: "inactive", viewValue: "Inactive" }
            ]
          },
          {
            filtertype: 'api',
            type: 'download',
            placeholder: 'Download',
            download: {
              downloadType: "api",
              extension: ".xlsx",
              api: {
                apiPath: "/products",
                apiType: ApiType.Loopback,
                responseType: "blob",
                method: "GET",
                params: new HttpParams()  
                  .append("order", JSON.stringify([["createdAt" ,"DESC"]]))
                  .append('include',JSON.stringify({all : true}))
                  .append('exportExcel', JSON.stringify({ headerDisplayType: 'propercase' ,keys :[
                    {
                      name : 'id',
                      transformedName:'Id'
                    },
                    {
                      name:'internalSku',
                      transformedName:'Internal SKU'
                    },
                    {
                      name:'externalSku',
                      transformedName:'Enternal SKU'
                    },
                    {
                      name:'name',
                      transformedName:'Name'
                    },
                    {
                      name:'price',
                      transformedName:'Price'
                    },
                    {
                      name:'sellingPrice',
                      transformedName:'Selling Price'
                    },
                    {
                      name:'thumbImages',
                      transformedName:'Thumb Images'
                    },
                    {
                      name:'specifications',
                      transformedName:'Specifications'
                    },
                    {
                      name:'availableStock',
                      transformedName:'Available Stock'
                    },
                    {
                      name:'status',
                      transformedName:'Status'
                    },
                    {
                      name:'color.name',
                      transformedName:'Color'
                    },
                    {
                      name:'masterProduct.name',
                      transformedName:'Master Product'
                    },
                    {
                      name:'masterProduct.internalSku',
                      transformedName:'Master product sku'
                    },
                    {
                      name:'vendor.id',
                      transformedName:'Vendor'
                    },
                    {
                      name:'createdBy.name',
                      transformedName:'Created By' 
                    },
                    {
                      name:'updatedBy.name',
                      transformedName:'Updated By'
                    },
                    {
                      name:'createdAt',
                      transformedName:'Created At'
                    },
                    {
                      name:'updatedAt',
                      transformedName:'Updated At ' 
                    },
                    {
                      name:'brandName',
                      transformedName:'Brand Name'
                    },
                    {
                      name:'category.name',
                      transformedName:'Category'
                    },
                    {
                      name:'description',
                      transformedName:'Description'
                    },
                    {
                      name:'installationCharge',
                      transformedName:'Installation Charge'
                    }
                  ] })),
              },
            }
          }
        ],
        api: {
          apiPath: '/master-products',
          method: 'GET'
        },
        countApi: {
          apiPath: '/master-products/count',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100 ,200]
        },
        header: 'Master Products'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateMasterProductComponent,
        inputData: [],
        width: 600,
        header: 'Add Master product'
      },
      permission: { type: 'single', match: { key: 'CREATE_PRODUCTS', value: true }}
            
    }
  }


}
