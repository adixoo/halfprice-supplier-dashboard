import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-builder',
  templateUrl: './create-builder.component.html',
  styleUrls: ['./create-builder.component.scss']
})
export class CreateBuilderComponent implements OnInit {

  constructor() { }

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;


  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'registeredName',
          type: 'text',
          placeholder: 'Registered Name',
          required: {
            value: true,
            errorMessage: 'Registered Name is required.'
          }
        },
        {
          name: 'logo',
          type: 'image',
          placeholder: 'Upload logo image',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'banner',
          type: 'image',
          placeholder: 'Upload Banner Image',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        

        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
        // {
        //   name: 'name',
        //   type: 'text',
        //   placeholder: 'Name',
        //   required: {
        //     value: true,
        //     errorMessage: 'Name is required.'
        //   }
        // },
        // {
        //   name: 'status',
        //   type: 'hidden',
        //   placeholder: 'Status',
        //   defaultValue:'active'
        // }
      ],
      submitApi: {
        apiPath: `/builders`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Builder created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
