import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBuilderComponent } from './create-builder.component';

describe('CreateBuilderComponent', () => {
  let component: CreateBuilderComponent;
  let fixture: ComponentFixture<CreateBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
