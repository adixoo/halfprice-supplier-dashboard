import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { BuilderDetailsComponent } from './builder-details/builder-details.component';
import { CreateBuilderComponent } from './create-builder/create-builder.component';
import { EditBuilderComponent } from './edit-builder/edit-builder.component';

@Component({
  selector: 'app-builder',
  templateUrl: './builders.component.html',
  styleUrls: ['./builders.component.scss']
})
export class BuildersComponent implements OnInit {

  constructor() { }
  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          },
          {
            header:'Registered Name',
            type:'text',
            value:'registeredName'
          },
          {
            header: 'Logo',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'logo'
                }
              ]
            }
          },
          {
            header: 'Banner',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'banner'
                }
              ]
            }
          },
          {
            header : 'Created At',
            type : 'date',
            value : 'createdAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          },
          {
            header : 'Updated At',
            type : 'date',
            value : 'updatedAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          }
          // {
          //   header:'Logo',
          //   type:'image',
          //   value:'logo'
          // }
        ],
        fieldActions: [
  
          {
            // action: {
            //   actionType: 'modal',
            //   modal: {
            //     component: BuilderDetailsComponent,
            //     inputData: [
            //       {
            //         inputKey: 'builderId',
            //         type: 'key',
            //         key: 'id'
            //       }
            //     ],
            //     header: 'Builder Details',
            //     width: 600
            //   }
            // },
            action: {
              actionType: 'link',
              link: {
                url: '/dashboard/builder/${id}',
                target: 'self',
                type: 'url'
              }
            },
            icon: {
              type: CanIconType.Flaticon,
              name: 'flaticon-eye',
              tooltip: 'View'
            }
          },
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditBuilderComponent,
                inputData: [
                  {
                    inputKey: 'builderId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Builder',
                width: 600
              }
            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Builder',
            }
          }
        ],
        filters: [
          // {
          //   filtertype: 'local',
          //   placeholder: 'Search',
          //   keys: ['name','registeredName']
          // },
          {
            filtertype: 'api',
            placeholder: 'Search',
            type: 'text',
            key: 'id',
            searchType: 'autocomplete',
            autoComplete: {
              type: 'api',
              apiValueKey: 'id',
              apiViewValueKey: 'name',
              autocompleteParamKeys: ['name','registeredName'],
              api: {
                apiPath: '/builders',
                method: 'GET'
              }
            },
          },
        ],
        api: {
          apiPath: '/builders',
          method: 'GET'
        },
        countApi: {
          apiPath: '/builders/count',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100 , 200]
        },
        header: 'Builders'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateBuilderComponent,
        inputData: [],
        width: 600,
        header: 'Add Builder'
      },
    }
  }

}
