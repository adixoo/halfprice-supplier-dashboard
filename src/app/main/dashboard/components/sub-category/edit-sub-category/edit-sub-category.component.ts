import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-sub-category',
  templateUrl: './edit-sub-category.component.html',
  styleUrls: ['./edit-sub-category.component.scss']
})
export class EditSubCategoryComponent implements OnInit {


  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() subCategoryId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'description',
          type: 'text',
          placeholder: 'Description',
          value:'description',
          required: {
            value: true,
            errorMessage: 'Descriptionis required.'
          }
        },
       
        // {
        //   name: 'banners',
        //   type: 'image',
        //   placeholder: 'Upload Banner Image',
        //   value:'banners',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'image/*',
        //     limit: 2,
        //     multipleSelect: true,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
        // {
        //   name: 'icon',
        //   type: 'image',
        //   placeholder: 'Upload ICON Image',
        //   value:'icon',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'image/*',
        //     limit: 1,
        //     multipleSelect: false,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
        

        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      getApi:{
        apiPath: `/sub-categories/${this.subCategoryId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/sub-categories/${this.subCategoryId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Sub categories updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
