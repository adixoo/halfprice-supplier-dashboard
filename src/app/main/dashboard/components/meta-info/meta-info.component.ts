import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateMetaInfoComponent } from './create-meta-info/create-meta-info.component';
import { EditMetaInfoComponent } from './edit-meta-info/edit-meta-info.component';

@Component({
  selector: 'app-meta-info',
  templateUrl: './meta-info.component.html',
  styleUrls: ['./meta-info.component.scss']
})
export class MetaInfoComponent implements OnInit {

  public tableData: CanTable;
  public fabButtonConfig: CanFabButton;
  constructor() { }

  ngOnInit() {
    this.tableData = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Product',
          type: 'text',
          value: 'product.name'
        },
        {
          header: 'Created At',
          type: 'date',
          value: 'createdAt',
          dateDisplayType: 'dd/MM/yyyy'
        },
        {
          header:'Updated At',
          type:'date',
          value :'updatedAt',
          dateDisplayType: 'dd/MM/yyyy'
        }
      ],
      fieldActions: [
     
        {
          action: {
            actionType: 'modal',
            modal: {
              component: EditMetaInfoComponent,
              inputData: [
                {
                  inputKey: 'metaId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Edit Meta Info',
              width: 600
            }
          },
          icon: {
            name: 'edit',
            tooltip: 'Edit Meta Info',
          }
        }
      ],
      api: {
        apiPath: '/meta-infos',
        method: 'GET',
        params : new HttpParams()
        .append('include', JSON.stringify([{  all: true  }]))
      },
      countApi:{
        apiPath:'/meta-infos/count',
        method:'GET'
      },
      apiDataKey: "data",
  
      countApiDataKey: 'data.count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: 'Meta Information',
    }
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Add Form'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateMetaInfoComponent,
        inputData: [],
        width: 600,
        header: 'Add Meta Info'
      },
  
    }
  }

}
