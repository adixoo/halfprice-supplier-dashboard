import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-meta-info',
  templateUrl: './edit-meta-info.component.html',
  styleUrls: ['./edit-meta-info.component.scss']
})
export class EditMetaInfoComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() metaId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'productId',
          type: 'select',
          placeholder: 'Product',
          value:'productId',
          select: {
            type: 'api',
            api: {
              apiPath: '/products',
              method: 'GET',
              params: new HttpParams()
              .append('status', 'active')
            },
            apiDataKey:'data',
            apiValueKey: "id",
            apiViewValueKey: "name"
          },              
        }, 
        
      {
        name: 'tnc',
        type: 'group',
        placeholder: 'Terms and Conditions',
        value:'tnc',
        formFields: [         
          {
            name: "type",
            type: "select",
            placeholder: "Type",
            values: [
              { value: 'html', viewValue: 'HTML' },
              { value: 'text', viewValue: 'TEXT' },
              { value: 'url', viewValue: 'URL' },
            ]
          },
          {
            name: 'data',
            type: 'textarea',
            placeholder: 'Data'
          }
        ]
      },
      {
        name: 'privacy',
        type: 'group',
        placeholder: 'Privacy',
        value:'privacy',
        formFields: [         
          {
            name: "type",
            type: "select",
            placeholder: "Type",
            values: [
              { value: 'html', viewValue: 'HTML' },
              { value: 'text', viewValue: 'TEXT' },
              { value: 'url', viewValue: 'URL' },
            ]
          },
          {
            name: 'data',
            type: 'textarea',
            placeholder: 'Data'
          }
        ]
      },
      {
        name: 'features',
        type: 'group',
        placeholder: 'Features',
        value:'features',
        formFields: [         
          {
            name: "type",
            type: "select",
            placeholder: "Type",
            values: [
              { value: 'html', viewValue: 'HTML' },
              { value: 'text', viewValue: 'TEXT' },
              { value: 'url', viewValue: 'URL' },
            ]
          },
          {
            name: 'data',
            type: 'textarea',
            placeholder: 'Data'
          }
        ]
      },
      {
        name: 'default',
        type: 'toggle',
        value:'default',
        placeholder: 'Default',
        defaultValue: false
      },
      ],
      getApi:{
        apiPath: `/meta-infos/${this.metaId}`,
        method:'GET'
      },
      apiDataKey: "data",
      submitApi: {
        apiPath: `/meta-infos/${this.metaId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "FAQ updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
