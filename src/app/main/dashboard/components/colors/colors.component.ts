import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateColorComponent } from './create-color/create-color.component';
import { EditColorComponent } from './edit-color/edit-color.component';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.scss']
})
export class ColorsComponent implements OnInit {

  constructor() { }
  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Hex Code',
            type: 'text',
            value: 'hex',
          },
          // {
          //   header:'Registered Name',
          //   type:'text',
          //   value:'registeredName'
          // },
          // {
          //   header:'Logo',
          //   type:'image',
          //   value:'logo'
          // }
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditColorComponent,
                inputData: [
                  {
                    inputKey: 'colorId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Color',
                width: 600
              },
      permission: { type: 'single', match: { key: 'UPDATE_COLORS', value: true }}

            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Color',
            }
          }
        ],
        filters: [
          {
            filtertype: 'api',
            placeholder: 'Search',
            type: 'text',
            key: 'id',
            searchType: 'autocomplete',
            autoComplete: {
              type: 'api',
              apiValueKey: 'id',
              apiViewValueKey: 'name',
              autocompleteParamKeys: ['name'],
              api: {
                apiPath: '/colors',
                method: 'GET'
              }
            },
          },
        ],
        api: {
          apiPath: '/colors',
          method: 'GET'
        },
        countApi: {
          apiPath: '/colors/count',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100 , 200]
        },
        header: 'Colors'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateColorComponent,
        inputData: [],
        width: 600,
        header: 'Add Color'
      },
      permission: { type: 'single', match: { key: 'CREATE_ROLES', value: true }}

    }
  }


}
