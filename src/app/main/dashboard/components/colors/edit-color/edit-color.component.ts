import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-color',
  templateUrl: './edit-color.component.html',
  styleUrls: ['./edit-color.component.scss']
})
export class EditColorComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() colorId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [        
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name:'hex',
          type:"text",
          placeholder:'Hex code',
          value:'hex',
          required: {
            value: true,
            errorMessage: 'Hex code is required.'
          }
        }
        // {
        //   name: 'status',
        //   type: 'select',
        //   placeholder: 'Status',
        //   value:'status',
        //   values: [
        //     { value: 'active', viewValue: 'active' },
        //     { value: 'inactive', viewValue: 'inactive' },
        //   ]
        // }
      ],
      getApi:{
        apiPath: `/colors/${this.colorId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/colors/${this.colorId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Color updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
