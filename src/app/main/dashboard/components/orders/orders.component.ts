import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  constructor() { }

  public tableConfig: CanTable;

  ngOnInit() {
 
    // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Order Number',
          type: 'text',
          value: 'orderNumber'
        },
        {
          header:'Date',
          type:'date',
          value:'createdAt'
        },
        {
          header:'Mobile',
          type:'text',
          value:'user.mobile'
        },
        {
          header:'Customer Name',
          type:'text',
          value:'user.name'
        },
        {
          header: 'Total Amount',
          type: 'text',
          value: 'totalAmount',
        },
        {
          header:'Payment Status',
          type:'text',
          value:'paymentStatus'
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        }
      ],
      filters: [
        // {
        //   filtertype: 'local',
        //   placeholder: 'Search',
        //   keys: ['orderNumber', 'user.name', 'user.mobile']
        // },
        {
          filtertype: 'api',
          placeholder: 'Search by order number',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'orderNumber',
            autocompleteParamKeys: ['orderNumber'],
            api: {
              apiPath: '/orders',
              method: 'GET',
              params:new HttpParams()
              .append('include', JSON.stringify([{ all: true }]))
            }
          },
        },
        {
          filtertype: 'api',
          placeholder: 'Search by customer name and mobile',
          type: 'text',
          key: 'userId',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'name',
            autocompleteParamKeys: ['name','mobile'],
            api: {
              apiPath: '/users',
              method: 'GET',
              params:new HttpParams()
              .append('include', JSON.stringify([{ all: true }]))
            }
          },
        },
        {
          filtertype: 'api',
          placeholder: 'Status',
          type: 'dropdown',
          key: 'status',
          // selectedValue: 'new',
          value: [
            { value: 'open', viewValue: 'Open' },
            { value: 'confirmed', viewValue: 'Confirmed' },
            { value: 'in_progress', viewValue: 'In progress' },
            { value: 'cancelled', viewValue: 'Cancelled' },
            { value: 'completed', viewValue: 'Completed' }

          ]
        },
        {
          filtertype: 'api',
          placeholder: 'Order Date',
          type: 'date',
          key: 'createdAt',
          date: {
            enabledRange: true,
            enabledTime: false,
            dateRange:{
              maxDate : new Date()
            }
          }
        },
        {
          filtertype: 'api',
          placeholder: 'Payment Status',
          type: 'dropdown',
          key: 'paymentStatus',
          // selectedValue: 'new',
          value: [
            { value: 'none', viewValue: 'none' },
            { value: 'booking', viewValue: 'booking' },
            { value: 'partial', viewValue: 'partial' },
            { value: 'full', viewValue: 'full' }

          ]
        },
      ],
      fieldActions: [
  
        // {
        //   action: {
        //     actionType: 'modal',
        //     modal: {
        //       component: EditConsumerComponent,
        //       inputData: [
        //         {
        //           inputKey: 'userId',
        //           type: 'key',
        //           key: 'id'
        //         }
        //       ],
        //       header: 'Edit Consumer',
        //       width: 600
        //     }
        //   },
        //   icon: {
        //     name: 'edit',
        //     tooltip: 'Edit Category',
        //   }
        // },
        {
          action: {
            actionType: 'link',
            link: {
              url: '/dashboard/orders/${id}/${cartId}',
              target: 'self',
              type: 'url'
            }
          },
          icon: {
            type: CanIconType.Flaticon,
            name: 'flaticon-eye',
            tooltip: 'View'
          }
        },
     
      ],
      api: {
        apiPath: '/orders',
        method: 'GET',
        params : new HttpParams()
        .append('include', JSON.stringify([{  all: true  }]))
        .append('order', JSON.stringify([['createdAt', 'DESC']]))
      },
      countApi: {
        apiPath: '/orders/count',
        method: 'GET',

      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100,200]
      },
      header: 'Orders'
    }
  }
}
