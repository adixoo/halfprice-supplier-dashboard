import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
@Injectable({providedIn : 'root'})
export class UpdateService{
    private _isFormSubmitted$ = new BehaviorSubject<boolean>(false);
    private _isTransactionAdded$ = new BehaviorSubject<boolean>(false);
    private _isFollowUpsAdded$ = new BehaviorSubject<boolean>(false);
    private _isOrderUpdated$ = new BehaviorSubject<boolean>(false);
    private _isRepresentativeAssigned$ = new BehaviorSubject<boolean>(false);
    private _isConversationAdded$ = new BehaviorSubject<boolean>(false);



    
    public get isFormSubmitted$() : Observable<boolean> {
        return this._isFormSubmitted$.asObservable();
    }
    
    public set isFormSubmitted(v : boolean) {
        this._isFormSubmitted$.next(v);
    }
    
    public get isTransactionAdded$() : Observable<boolean> {
        return this._isTransactionAdded$.asObservable();
    }
    
    public set isTransactionAdded(v : boolean) {
        this._isTransactionAdded$.next(v);
    }

    public get isOrderUpdated$() : Observable<boolean> {
        return this._isOrderUpdated$.asObservable();
    }
    
    public set isOrderUpdated(v : boolean) {
        this._isOrderUpdated$.next(v);
    }

    public get isFollowUpsAdded$() : Observable<boolean> {
        return this._isFollowUpsAdded$.asObservable();
    }
    
    public set isFollowUpsAdded(v : boolean) {
        this._isFollowUpsAdded$.next(v);
    }
    
    public get isRepresentativeAssigned$() : Observable<boolean> {
        return this._isRepresentativeAssigned$.asObservable();
    }
    
    public set isRepresentativeAssigned(v : boolean) {
        this._isRepresentativeAssigned$.next(v);
    }

    public get isConversationAdded$() : Observable<boolean> {
        return this._isConversationAdded$.asObservable();
    }
    
    public set isConversationAdded(v : boolean) {
        this._isConversationAdded$.next(v);
    }
}