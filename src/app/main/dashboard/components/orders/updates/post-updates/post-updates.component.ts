import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CanForm } from 'src/@can/types/form.type';
import { UpdateService } from '../update.service';

@Component({
  selector: 'app-post-updates',
  templateUrl: './post-updates.component.html',
  styleUrls: ['./post-updates.component.scss']
})
export class PostUpdatesComponent implements OnInit {
  formData: CanForm;
  orderId:any;
  userId: any;
  order:any;
 @Output() outputEvent = new EventEmitter<boolean>(false);
 constructor(
  private updateService : UpdateService,
  public dialogRef: MatDialogRef<PostUpdatesComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any) {
    this.order = this.data.order
  }
 
  ngOnInit() {    
    this.userId = this.order.userId;
    this.orderId = this.order.id
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,     
      sendAll : true,
      formFields: [
        
        {
          name: 'remarks',
          type: 'textarea',
          placeholder: 'Enter message',
          required: {
            value: true,
            errorMessage: 'message is required.'
          }
        },
        {
          name: 'images',
          type: 'image',
          placeholder: 'Select Images',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 2,
            multipleSelect: true,
            
          },
          suffixIcon: { name: 'attach_file' },
        },

        {
          name: 'progress',
          type: 'select',
          placeholder: 'Progress Status',
          select: {
            type: 'api',
            api: {
              apiPath: '/statuses',
              method: 'GET'
            },
            apiValueKey: "name",
            apiViewValueKey: "name"
          },
          required: {
            value: true,
            errorMessage: 'Progress Status'
          },
          relativeSetFields:[{
            key:'progressStatus',
            type:'different',
            value :'orderStatus',
            differentType:'dataSource'
          },
          {
            key:'icon',
            type:'different',
            value :'icon',
            differentType:'dataSource'
          },

          ]
          
        },
        {
          name:'icon',
          type:'hidden',
          value:'icon',
          placeholder:null
        },
        {
          name:'progressStatus',
          type:'hidden',
          value:'orderStatus',
          placeholder: null,
        },

        {
          name:'orderId',
          type : 'hidden',
          placeholder:null,
          defaultValue: this.orderId,
          // send: 'alwaysSend'
        },
        {
          name :'userId',
          type : 'hidden',
          placeholder : null,
          defaultValue: this.userId,
          // send: 'alwaysSend'
        },

        
      ],
      submitApi: {
        apiPath: `/progresses`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'POST'
      },
      submitSuccessMessage: "Order Status update successfully!"
    }
  }

  formSubmitted(event: boolean) { 
    this.outputEvent.emit(event);
    this.updateService.isFormSubmitted = true;
    this.closeModal();
  }
  closeModal(){
    this.dialogRef.close();
  }
  getData(progress){
    this.userId = progress.userId;
    this.orderId = progress.orderId
  }
}
