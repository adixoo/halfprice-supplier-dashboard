import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousUpdatesComponent } from './previous-updates.component';

describe('PreviousUpdatesComponent', () => {
  let component: PreviousUpdatesComponent;
  let fixture: ComponentFixture<PreviousUpdatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviousUpdatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousUpdatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
