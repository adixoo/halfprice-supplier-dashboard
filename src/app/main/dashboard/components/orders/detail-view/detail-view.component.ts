import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class DetailViewComponent implements OnInit {

  public tableConfig: CanTable;
  @Input() order:any;
  public orderItems:any;

  constructor(private apiService: CanApiService) { }

  async ngOnInit() {
   await this.getOrderItems();
  }

  getOrderItems(){
    return new Promise((resolve) => {
      const orderApi: CanApi = {
        apiPath: `/orders/items?id=${this.order.id}`,
        method: 'GET',
        // params: new HttpParams()
        // .append('id',this.order.id)
      }
      this.apiService.request(orderApi)
        .subscribe((items:any[]) => {
          this.orderItems = items;
          })
    })
  }
}
