import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';
import { UpdateService } from '../updates/update.service';

@Component({
  selector: 'app-representative',
  templateUrl: './representative.component.html',
  styleUrls: ['./representative.component.scss']
})
export class RepresentativeComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;
  @Input() orderId:any;

  constructor(
    private updateService : UpdateService,

  ) { }

  ngOnInit() {
    // console.log(this.order.id)
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [ 
        {
          name: 'user',
          type: 'select',
          placeholder: 'Select User',
          value: 'user',
          select: {
            type: 'api',
            api: {
              apiPath: '/users',
              method: 'GET',
              params: new HttpParams().set('type', 'internal')
            },
            apiValueKey: "id",
            apiViewValueKey: "name"
          },
          required: {
            value: true,
            errorMessage: 'User is required!'
          },
          send: 'notSend',
          relativeSetFields:[{
            key:'representativeId',
            type:'different',
            value :'id',
            differentType:'dataSource'
          }],
        },
        {
          name:'representativeId',
          type: 'hidden',
          value :'id',
          placeholder:null
        },

      ],
      getApi:{
        apiPath: `/orders/${this.orderId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/orders/${this.orderId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Representative Assign successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
    this.updateService.isRepresentativeAssigned = true;
  }
}

