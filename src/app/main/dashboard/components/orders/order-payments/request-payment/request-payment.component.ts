import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanNotificationService } from 'src/@can/services/notification/notification.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanForm } from 'src/@can/types/form.type';
import { CanButton } from 'src/@can/types/shared.type';

@Component({
  selector: 'app-request-payment',
  templateUrl: './request-payment.component.html',
  styleUrls: ['./request-payment.component.scss']
})
export class RequestPaymentComponent implements OnInit {

  // formData: CanForm;
  public amount:any;
  @Input() paidAmount:number;
  @Input() order:any;
  public validAmount:boolean = true;
  // @Output() outputEvent = new EventEmitter<boolean>(false);
 public requestPaymentConfig : CanButton;
  constructor(private apiService: CanApiService, private notificationService: CanNotificationService){

  }
  
   ngOnInit() {
     if(this.paidAmount >= this.order.totalAmount){
       this.validAmount = false
     }
    //  this.amount = this.order.totalAmount -this.paidAmount;
      this.requestPaymentConfig = {
      label : 'Request payment',
      type : 'raised',
      color : 'primary',
      float:'right'
    }
    //  this.formData = {
    //    type: 'create',
    //    discriminator: 'formConfig',
    //    columnPerRow: 1, 
    //    header:'Request Payment',    
    //    sendAll : true,
    //    formFields: [
         
    //      {
    //        name: 'amount',
    //        type: 'number',
    //        placeholder: 'Enter amount'
    //      },
        
 
    //      {
    //        name:'orderId',
    //        type : 'hidden',
    //        placeholder:null,
    //        defaultValue: this.orderId,
    //        // send: 'alwaysSend'
    //      },
 
         
    //    ],
    //    submitApi: {
    //      apiPath: `/progresses`,
    //      method: 'POST'
    //    },
    //    formButton:
    //    {
    //      type: 'raised',
    //      color: 'primary',
    //      label: 'Request Payment'
    //    },
    //    submitSuccessMessage: "Payment requested successfully!"
    //  }
   }
 
  //  formSubmitted(event: boolean) { 
  //    this.outputEvent.emit(event);
  //  }

  validateAmount(){
    if((this.amount + this.paidAmount) > this.order.totalAmount 
     || (this.amount < 0 || this.amount == 0)
     ){
      this.validAmount = false
    }else{
      this.validAmount = true
    }
  }
   requestPayment(){
    if(this.amount && this.validAmount){

    const generateLink: CanApi = {
      apiPath: `/payments/generate?orderId=${this.order.id}`,
      method: "GET",
    };
    if(this.amount){
      generateLink.apiPath +=`&amount=${this.amount}`
    }
    this.apiService.request(generateLink).subscribe((res: any[]) => {
      // this.order = res;
      this.notificationService.showSuccess(res['message']);
      this.amount= null;
  });
    }
   }
}
