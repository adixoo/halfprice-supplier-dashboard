import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CanForm } from 'src/@can/types/form.type';
import { UpdateService } from '../../updates/update.service';

@Component({
  selector: 'app-record-payment',
  templateUrl: './record-payment.component.html',
  styleUrls: ['./record-payment.component.scss']
})
export class RecordPaymentComponent implements OnInit {

     
    
  formData: CanForm;
  order:any;
  // @Input() order:any;
  @Output() outputEvent = new EventEmitter<boolean>(false);
 
  constructor(
    private updateService : UpdateService,
    public dialogRef: MatDialogRef<RecordPaymentComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any) {
    this.order = this.data.order
  }
  
   ngOnInit() {
     this.formData = {
       type: 'create',
       discriminator: 'formConfig',
       columnPerRow: 1, 
      //  header:'Record Payment ',    
       sendAll : true,
       formFields: [
         
         {
           name: 'amount',
           type: 'number',
           placeholder: 'Enter amount',
           pattern:{
             value:'^([1-9]|[0-9]{2,})$',
             errorMessage:'Please enter valid amount'
           },
           relativeSetFields:[{
            key:'totalAmount',
            type:'same',
            value :'amount'
          }]
         },
        
         {
          name: 'totalAmount',
          type: 'hidden',
          placeholder: null,
          value:'amount'
        },
        {
          name: 'platform',
          type: 'hidden',
          placeholder: 'Platform',
          defaultValue:'WEB'
          // values: [
          //   { value: 'WEB', viewValue: 'WEB' },
          //   { value: 'APP', viewValue: 'APP' },
          // ],
          // required: {
          //   value: true,
          //   errorMessage: 'Platform is required.'
          // }
        },
        {
          name: 'paymentGatewayId',
          type: 'select',
          placeholder: 'Payment Gateways',
          select: {
            type: 'api',
            api: {
              apiPath: '/payment-gateways',
              method: 'GET'
            },
            apiValueKey: "id",
            apiViewValueKey: "pgName"
          },
          required: {
            value: true,
            errorMessage: 'Payment Gateway is required!'
          }
        },
        {
          name:'orderNumber',
          type:'hidden',
          placeholder : null,
          defaultValue:this.order.orderNumber
        },
        {
          name:'transactionStatus',
          type:'hidden',
          placeholder : null,
          defaultValue:'TXN_SUCCESS'
        },
         {
           name:'orderId',
           type : 'hidden',
           placeholder:null,
           defaultValue: this.order.id,
           // send: 'alwaysSend'
         },
         {
          name:'transactionType',
          type : 'hidden',
          placeholder:null,
          defaultValue: 'manual',
          // send: 'alwaysSend'
        },
         
       ],
       submitApi: {
         apiPath: `/transactions`,
         method: 'POST'
       },
       formButton:
       {
         type: 'raised',
         color: 'primary',
         label: 'Record Payment'
       },
       submitSuccessMessage: " Payment recorded successfully!"
     }
   }
 
   closeModal(){
    this.dialogRef.close();
  }

   formSubmitted(event: boolean) { 
     this.outputEvent.emit(event);
    this.updateService.isTransactionAdded = true;
    this.closeModal();

    // this.updateService.isOrderUpdated = true;


   }

}
