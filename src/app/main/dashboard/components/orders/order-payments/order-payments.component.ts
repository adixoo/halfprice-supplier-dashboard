import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanButton } from 'src/@can/types/shared.type';
import { UpdateService } from '../updates/update.service';
import { RecordPaymentComponent } from './record-payment/record-payment.component';

@Component({
  selector: 'app-order-payments',
  templateUrl: './order-payments.component.html',
  styleUrls: ['./order-payments.component.scss']
})
export class OrderPaymentsComponent implements OnInit {

  @Input() order:any;
  constructor(
    private apiService: CanApiService,
    private updateService : UpdateService,
    public dialog: MatDialog ) { }
  public paidAmount:number = 0;
  private subscription:Subscription;
  dialogRef: MatDialogRef<RecordPaymentComponent, any>; 
  public recordPaymentBtnConfig : CanButton;


  // public generateBtnConfig : CanButton;
   ngOnInit() {
    this.recordPaymentBtnConfig = {
      label : 'Record Payment',
      type : 'stroked',
      color : 'primary',
      float:'left'
    }
    // this.generateBtnConfig = {
    //   label : 'Generate',
    //   type : 'raised',
    //   color : 'primary',
    //   float:'left'
    // }
    this.subscription =  this.updateService.isTransactionAdded$.subscribe(async (value) =>{
      if(value){
        this.getTransactions()
      }
    })
     this.getTransactions()

  }

  openPostModal(){
    // this.notificationService.showSuccess();
    const dialogRef = this.dialog.open(RecordPaymentComponent,
      {
        width: '700px',
        data: { order: this.order }
      }
    );
    dialogRef.afterClosed().subscribe(submitResponse => {
    });
    // return new Promise((resolve, reject) => {
    //   const dialogRef = this.dialog.open(PostUpdatesComponent, {
    //     disableClose: true,
    //     data: [{

    //     }]
    //   });
    //   dialogRef.afterClosed().subscribe((dialogResult) => {
    //     resolve(dialogResult);
    //   });
    // })
  }
  getTransactions(){
    return new Promise((resolve, reject) => {
      const getTransaction: CanApi = {
        apiPath: `/transactions`,
        method: "GET",
        params: new HttpParams()
        .append('transactionStatus','TXN_SUCCESS')
        .append('orderId',this.order.id)
      };
      this.apiService.request(getTransaction).subscribe((transaction: any[]) => {
        this.paidAmount = 0;  
        for (let index = 0; index < transaction.length; index++) {
          this.paidAmount += transaction[index].amount       
        }
        resolve(this.paidAmount)
    });
     
    });
   
  
   }

   ngOnDestroy(){
    if(this.subscription && !this.subscription.closed){
      this.subscription.unsubscribe();
    }
  }

 
}
