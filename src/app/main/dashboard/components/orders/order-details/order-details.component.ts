import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanAuthService } from 'src/@can/services/auth/auth.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanButton, CanIconType } from 'src/@can/types/shared.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { CanTable } from 'src/@can/types/table.type';
import { BasicInformationComponent } from '../basic-information/basic-information.component';
import { DetailViewComponent } from '../detail-view/detail-view.component';
import { OrderPaymentsComponent } from '../order-payments/order-payments.component';
import { UpdateService } from '../updates/update.service';
import { UpdatesComponent } from '../updates/updates.component';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  public orderId:any;  
  public cartId:any;
  public businessId:any;
  public order:any;
  public invoice:any;
  public cartDetails:any;
  private subscription:Subscription;
  public isLoad:boolean = false;

  constructor(private activedRoute: ActivatedRoute,
       private apiService: CanApiService,
       private updateService : UpdateService,
       private authService : CanAuthService) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => {
      this.orderId = parseInt(paramMap.get('id'))
      this.cartId = parseInt(paramMap.get('cartId'))
    });
    this.businessId = this.authService.getBusinessId()
  }

  public tabViewConfig: CanTabView;
  public detailViewData: CanDetailView;
  public tableConfig: CanTable;
  public invoiceConfig: CanTable;

  public generateInvoiceBtn : CanButton;

  async ngOnInit() {
    this.subscription =  this.updateService.isTransactionAdded$.subscribe(async (value) =>{
      // this.refreshEvent.next(value)
      if(value){
        await this.getOrder();
      }
    })
    this.generateInvoiceBtn = {
      label : 'Generate Invoice',
      type : 'stroked',
      color : 'primary',
      float:'right'
    }
    this.detailViewData = {
          discriminator: 'detailViewConfig',
          columnPerRow: 5,
          labelPosition: 'top',
          dataSource:this.invoice,
          displayedFields:[
            {
              header:'Invoice Date',
              type:'date',
              value:'invoiceDate'
            },
            {
              header: 'invoice Number',
              type:'text',
              value:'invoiceNumber'
            },
            {
              header: 'Invoice File',
              type: 'document',
              documents: {
                showAll: true,
                openType: 'link',
                documentItems:[{
                  type:'api',
                  value:'invoiceUrl',
                  // openType:'modal'
                }]
              }
            },
          ]
    }
       
    await this.getCartDetails()
    await this.getOrder();
  // Table Data Init
  this.tableConfig = {
    discriminator: 'tableConfig',
    dataSource: this.cartDetails,
    displayedColumns: [
      {
        header: 'Product',
        type: 'text',
        value: 'product.name'
      },
      {
        header: 'Quantity',
        type:'text',
        value:'quantity'
      },
      {
        header:'description',
        type:'text',
        value:'product.description'
      },
      {
        header:'Selling Price',
        type:'text',
        value:'product.sellingPrice'
      },
      {
        header: 'price',
        type: 'text',
        value: 'product.price',
      },
      {
        header: 'Status',
        type: 'text',
        value: 'status',
      }
    ],
    api: {
      apiPath: '/orders',
      method: 'GET',
      params : new HttpParams()
      .append('include', JSON.stringify([{  all: true  }]))
      .append('order', JSON.stringify([['createdAt', 'DESC']]))
    },
    countApi: {
      apiPath: '/orders/count',
      method: 'GET',

    },
    countApiDataKey: 'count',
    pagination: {
      pageSizeOptions: [100,200]
    },
    header: 'Orders Datails'
  }
 // Table Data Init
 this.invoiceConfig = {
  discriminator: 'tableConfig',
  displayedColumns: [
    {
      header: 'Invoice Number',
      type:'text',
      value:'invoiceNumber'
    },
    {
      header:'Invoice Date',
      type:'date',
      value:'invoiceDate'
    },
    
    {
      header: 'Invoice File',
      type: 'document',
      documents: {
        showAll: true,
        openType: 'link',
        documentItems:[{
          type:'api',
          value:'invoiceUrl',
          // openType:'modal'
        }]
      }
    },
  ],
  api: {
    apiPath: '/invoices',
    method: 'GET',
    params : new HttpParams()
    .append('include', JSON.stringify([{  all: true  }]))
    .append('order', JSON.stringify([['createdAt', 'DESC']]))
    .append('cartId',this.cartId)
    .append('businessId',this.businessId)
  },
  countApi: {
    apiPath: '/invoices/count',
    method: 'GET',

  },
  countApiDataKey: 'count',
  pagination: {
    pageSizeOptions: [100,200]
  },
  header: 'Invoice'
}
  }
  generateInvoice(){
    return new Promise((resolve, reject) => {
      const generateInvoice: CanApi = {
        apiPath: `/invoices/generate/${this.cartId}/${this.businessId}`,
        method: "GET"
      };
      this.apiService.request(generateInvoice).subscribe((res: any[]) => {
        this.cartDetails = res;
        resolve(res);
      });
    });
  }
  getOrder(){
    return new Promise((resolve, reject) => {
      const getProperties: CanApi = {
        apiPath: `/orders/${this.orderId}`,
        method: "GET",
        params: new HttpParams()
        .append('include', JSON.stringify([{ all: true }]))
      };
      this.apiService.request(getProperties).subscribe((res: any[]) => {
        this.order = res;
        resolve(res);
      });
    });
  }

  getCartDetails(){
    return new Promise((resolve, reject) => {
      const getCartDetails: CanApi = {
        apiPath: `/cart-details`,
        method: "GET",
        params: new HttpParams()
        .append('include', JSON.stringify([{ all: true }]))
        .append('cartId',this.cartId)
        .append('businessId',this.businessId)
      };
      this.apiService.request(getCartDetails).subscribe((res: any[]) => {
        this.cartDetails = res;
        resolve(res);
      });
    });
  }

  // ngOnDestroy(){
  //   if(this.subscription && !this.subscription.closed){
  //     this.subscription.();
  //   }
  // }
}
