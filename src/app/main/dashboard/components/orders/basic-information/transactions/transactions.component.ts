import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  public tableConfig: CanTable;
  @Input() order:any;

  constructor() { }

  ngOnInit() {
       // Table Data Init
       this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'ID',
            type: 'text',
            value: 'orderNumber'
          },
          {
            header: 'Amount',
            type: 'text',
            value: 'amount',
          },
          {
            header:"Date-Time",
            type :'date',
            value :"createdAt"
          },
          {
            header:'Mode',
            type :'text',
            value :'paymentGateway.pgName'
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          }
        ],
        fieldActions: [
    
          // {
          //   action: {
          //     actionType: 'modal',
          //     modal: {
          //       component: EditConsumerComponent,
          //       inputData: [
          //         {
          //           inputKey: 'userId',
          //           type: 'key',
          //           key: 'id'
          //         }
          //       ],
          //       header: 'Edit Transactions',
          //       width: 600
          //     }
          //   },
          //   icon: {
          //     name: 'edit',
          //     tooltip: 'Edit Transactions',
          //   }
          // },
        ],
        api: {
          apiPath: '/transactions',
          method: 'GET',
          params : new HttpParams()
          .append('include',JSON.stringify([{all : true}]))
          .append('orderId', this.order.id)
        },
        countApi: {
          apiPath: '/transactions/count',
          method: 'GET',
          params : new HttpParams().set('orderId', this.order.id)
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100, 200]
        },
        header: 'Transactions'
      }
  }

}
