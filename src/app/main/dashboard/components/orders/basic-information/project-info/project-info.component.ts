import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { RepresentativeComponent } from '../../representative/representative.component';

@Component({
  selector: 'app-project-info',
  templateUrl: './project-info.component.html',
  styleUrls: ['./project-info.component.scss']
})
export class ProjectInfoComponent implements OnInit {

  public detailViewData: CanDetailView;
  @Input() order: any;
  public fabButtonConfig: CanFabButton;
  constructor() { }

  ngOnInit() {
    
       // Detail View Config Init
       this.detailViewData = {
        discriminator: 'detailViewConfig',
        columnPerRow: 1,
        labelPosition: 'top',
        // dataSource:this.order,
        displayedFields: [
          {
            header: null,
            type: 'text',
            value: 'name',
          },
          {
            header: null,
            type: 'text',
            value: 'builder.name',
          },
          {
            header:null,
            type :'text',
            value :'address.address'
          }
          
        ],
        api:{
          apiPath: `/projects/${this.order.projectId}`,
          method:'GET',
          params : new HttpParams().append('include',JSON.stringify({all : true}))
        }
       }
    

  }

  


}
