import { Component, Input, OnInit } from '@angular/core';
import { CanTable } from 'src/@can/types/table.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { RepresentativeComponent } from '../../representative/representative.component';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  public tableConfig: CanTable;
  @Input() items: any;
  public fabButtonConfig: CanFabButton;
  

  constructor() { }
  
  ngOnInit() {
    
       // Table Data Init
       this.tableConfig = {
        discriminator: 'tableConfig',
        elevation: 1,
        hideRefreshButton: true,
        dataSource: this.items,
        displayedColumns: [
          {
            header: 'SKU',
            type: 'text',
            value: 'internalSku'
          },
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header:"Quantity",
            type :'text',
            value :"quantity"
          },
          {
            header:'Brand',
            type :'text',
            value :'brandName'
          },
          {
            header: 'Unit Price',
            type: 'text',
            value: 'sellingPrice',
          },
          {
            header :'Vendor',
            type :'text',
            value : 'vendor.name'
          }
        ],
        fieldActions: [
    
          // {
          //   action: {
          //     actionType: 'modal',
          //     modal: {
          //       component: EditConsumerComponent,
          //       inputData: [
          //         {
          //           inputKey: 'userId',
          //           type: 'key',
          //           key: 'id'
          //         }
          //       ],
          //       header: 'Edit Transactions',
          //       width: 600
          //     }
          //   },
          //   icon: {
          //     name: 'edit',
          //     tooltip: 'Edit Transactions',
          //   }
          // },
        ],
        apiDataKey:'packageDetails',
        // api: {
        //   apiPath: `/orders/${this.order}`,
        //   method: 'GET',
        // },
        // countApi: {
        //   apiPath: '/transactions/count',
        //   method: 'GET',
  
        // },
        // countApiDataKey: 'count',
        // pagination: {
        //   pageSizeOptions: [100, 200]
        // },
        // header: 'Items'
       }
    
                  // Fab Button Config
                  this.fabButtonConfig = {
                    icon: {
                      name: 'add',
                      tooltip: 'Assign Representative'
                    },
                    type: 'modal',
                    position: CanFabButtonPosition.BottomRight,
                    modal: {
                      component: RepresentativeComponent,
                      inputData: [],
                      width: 600,
                      header: 'Assign Representative'
                    },
                  };
  }
}
