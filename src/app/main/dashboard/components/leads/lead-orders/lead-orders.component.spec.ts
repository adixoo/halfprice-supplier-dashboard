import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadOrdersComponent } from './lead-orders.component';

describe('LeadOrdersComponent', () => {
  let component: LeadOrdersComponent;
  let fixture: ComponentFixture<LeadOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
