import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-lead-orders',
  templateUrl: './lead-orders.component.html',
  styleUrls: ['./lead-orders.component.scss']
})
export class LeadOrdersComponent implements OnInit {

  constructor() { }

  @Input() userId:number;

  public tableConfig: CanTable;

  ngOnInit() {
 
    // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Order Number',
          type: 'text',
          value: 'orderNumber'
        },
        {
          header:'Date',
          type:'date',
          value:'createdAt'
        },
        {
          header:'Mobile',
          type:'text',
          value:'user.mobile'
        },
        {
          header:'Customer Name',
          type:'text',
          value:'user.name'
        },
        {
          header:'Configuration',
          type:'text',
          value:'configuration.name'
        },
        // {
        //   header: 'Total Amount',
        //   type: 'text',
        //   value: 'totalAmount',
        // },
        {
          header:'Payment Status',
          type:'text',
          value:'paymentStatus'
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        }
      ],
      
      fieldActions: [
  
        // {
        //   action: {
        //     actionType: 'modal',
        //     modal: {
        //       component: EditConsumerComponent,
        //       inputData: [
        //         {
        //           inputKey: 'userId',
        //           type: 'key',
        //           key: 'id'
        //         }
        //       ],
        //       header: 'Edit Consumer',
        //       width: 600
        //     }
        //   },
        //   icon: {
        //     name: 'edit',
        //     tooltip: 'Edit Category',
        //   }
        // },
        {
          action: {
            actionType: 'link',
            link: {
              url: '/dashboard/order/${id}',
              target: 'self',
              type: 'url'
            }
          },
          icon: {
            type: CanIconType.Flaticon,
            name: 'flaticon-eye',
            tooltip: 'View'
          }
        },
     
      ],
      api: {
        apiPath: '/orders',
        method: 'GET',
        params : new HttpParams()
        .append('order', JSON.stringify([['createdAt', 'DESC']]))
        .append('userId',this.userId.toString())

      },
      countApi: {
        apiPath: '/orders/count',
        method: 'GET',

      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100 , 200]
      },
      header: 'Orders'
    }
  }

}
