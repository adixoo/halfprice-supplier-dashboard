import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanNotificationService } from 'src/@can/services/notification/notification.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanForm } from 'src/@can/types/form.type';
import { UpdateService } from '../../../orders/updates/update.service';

@Component({
  selector: 'app-create-follow-ups',
  templateUrl: './create-follow-ups.component.html',
  styleUrls: ['./create-follow-ups.component.scss']
})
export class CreateFollowUpsComponent implements OnInit {


 @Output() outputEvent = new EventEmitter<boolean>(false);
  leadId:number;
  formData: CanForm;

  constructor(
    private apiService: CanApiService,
    private updateService : UpdateService,
    private notificationService : CanNotificationService,
    public dialogRef: MatDialogRef<CreateFollowUpsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.leadId = this.data.leadId
    }
   

   async ngOnInit() {    
        this.getFollowUps();
 
      this.formData = {
        type: 'create',
        discriminator: 'formConfig',
        columnPerRow: 1,     
        sendAll : true,
        header:'Create Follow ups',
        formFields: [
          
          {
            name:'schedule',
            placeholder:'Schedule',
            type:'date',
            date:{
              enabledTime:true,
              dateRange:{
                minDate: new Date()
              }
            }
          },
          {
            name:'leadId',
            placeholder:null,
            type:'hidden',
            defaultValue:this.leadId
          }
          
        ],
        submitApi: {
          apiPath: `/follow-ups`,
          method: 'POST'
        },
        formButton:
        {
          type: 'raised',
          color: 'primary',
          label: 'Save'
        },
        submitSuccessMessage: "Follow ups created successfully!"
      }

    
    }
    getFollowUps(){
      return new Promise((resolve , reject) =>{
        const getFollowUps:CanApi = {
          apiPath:`/follow-ups`,
          method:'GET',
          params: new HttpParams().append('leadId',this.leadId.toString()).append('status','pending')
        }
        this.apiService.request(getFollowUps).subscribe((res:any[]) =>{
          if(res && res.length){
            this.notificationService.showError('Before creating a new follow up, please close other pending follow ups.')
            this.closeModal()
          }
          resolve(res)
        })
      })
   
    }
    formSubmitted(event: boolean) { 
      this.outputEvent.emit(event);
      // this.updateService.isFollowUpsAdded = true;
      this.closeModal();
    }
    closeModal(){
      this.dialogRef.close();
    }
    updateLead(data){
      const submitApi:CanApi = {
        apiPath:`/leads/${this.leadId}`,
        method:'PATCH'
      }
      const bodyData ={
        nextFollowup : data.schedule
      }
      this.apiService.request(submitApi,bodyData).subscribe(res =>{
        if(res){
      this.updateService.isFollowUpsAdded = true;
        
        }
      })
    }

}
