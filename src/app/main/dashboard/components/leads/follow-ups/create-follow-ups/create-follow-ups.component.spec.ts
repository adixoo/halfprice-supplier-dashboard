import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFollowUpsComponent } from './create-follow-ups.component';

describe('CreateFollowUpsComponent', () => {
  let component: CreateFollowUpsComponent;
  let fixture: ComponentFixture<CreateFollowUpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFollowUpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFollowUpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
