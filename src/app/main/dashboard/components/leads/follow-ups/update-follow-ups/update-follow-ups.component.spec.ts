import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFollowUpsComponent } from './update-follow-ups.component';

describe('UpdateFollowUpsComponent', () => {
  let component: UpdateFollowUpsComponent;
  let fixture: ComponentFixture<UpdateFollowUpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFollowUpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFollowUpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
