import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanTable } from 'src/@can/types/table.type';
import { UpdateService } from '../../orders/updates/update.service';
import { CreateConversationsComponent } from '../create-conversations/create-conversations.component';

@Component({
  selector: 'app-conversations',
  templateUrl: './conversations.component.html',
  styleUrls: ['./conversations.component.scss']
})
export class ConversationsComponent implements OnInit {

  constructor(
    private updateService : UpdateService
  ) { }

  @Input() leadId:number;
  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;
  refreshEvent = new EventEmitter<boolean>(true);


  ngOnInit() {
    this.updateService.isConversationAdded$.subscribe(value => {
      this.refreshEvent.next(value)
    })
    // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      refreshEvent : this.refreshEvent,
      displayedColumns:[
        {
          header: 'Channel',
          type: 'text',
          value: 'channel'
        },
        {
          header: 'Type',
          type: 'text',
          value: 'type'
        },
        {
          header: 'Comment',
          type: 'text',
          value: 'comment',
          wordWrap:true
        },
        {
          header: 'Disposition',
          type: 'text',
          value: 'disposition'
        },
        {
          header: 'Agent Name',
          type: 'text',
          value: 'createdBy.name'
        },
        {
          header:'Created At',
          type:'date',
          value:'createdAt',
          dateDisplayType:'dd/MM/yyyy HH:mm:SS'
        }
     
      ],
      // fieldActions: [

        
        
      //   {
      //     action: {
      //       actionType: 'link',
      //       link: {
      //         url: '/dashboard/internal/${id}',
      //         target: 'self',
      //         type: 'url'
      //       }
      //     },
      //     icon: {
      //       type: CanIconType.Flaticon,
      //       name: 'flaticon-eye',
      //       tooltip: 'View'
      //     }
      //   },
      // ],
     
      api: {
        apiPath: '/conversations',
        method: 'GET',
        params: new HttpParams()
        .append('include',JSON.stringify({all : true}))
        .append('order', JSON.stringify([['createdAt', 'DESC']]))
        .append('leadId',this.leadId.toString())
      },
      countApi: {
        apiPath: '/conversations/count',
        method: 'GET',
      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100 ,200]
      },
      header: 'Conversations'
    }

        // Fab Button Config
        this.fabButtonConfig = {
          icon: {
            name: 'message',
            tooltip: 'Comments'
          },
          type: 'modal',
          position: CanFabButtonPosition.BottomRight,
          modal: {
            component: CreateConversationsComponent,
            inputData: [{
              type:'fixed',
              // key:'leadId',
              inputKey:'leadId',
              value:this.leadId
            }],
            width: 600,
            header: 'Post an update'
          },
        }
  }

}
