import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanForm } from 'src/@can/types/form.type';
import { UpdateService } from '../../../orders/updates/update.service';

@Component({
  selector: 'app-add-conversation',
  templateUrl: './add-conversation.component.html',
  styleUrls: ['./add-conversation.component.scss']
})
export class AddConversationComponent implements OnInit {

  constructor(
    public apiService : CanApiService,
    private updateService : UpdateService
  ) { }

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() leadId:number;
  formConfig: CanForm;
  private leadStatusId : number;
  ngOnInit() {
    this.formConfig = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      sendAll:true,
      formFields: [
        {
          name: 'channel',
          type: 'select',
          placeholder: 'Channel',
          values: [
            { value: 'call', viewValue: 'Call' },
            { value: 'chat', viewValue: 'Chat' },
            { value: 'email', viewValue: 'Email' },
            { value: 'whatsapp', viewValue: 'Whatsapp' },
          ]
        },
          {
            name:'leadId',
            placeholder:null,
            type:'hidden',
            defaultValue:this.leadId
          },
        {
          name: 'type',
          type: 'select',
          placeholder: 'Type',
          values: [
            { value: 'outbound', viewValue: 'Outbound' },
            { value: 'inbound', viewValue: 'Inbound' },
          ]
        },
        {
          name: 'comment',
          type: 'textarea',
          placeholder: 'Conversation',
          required: {
            value: true,
            errorMessage: 'Conversation is required.'
          }
        }, 
            {
              name: 'leadStatusId',
              type: 'select',
              placeholder: 'Lead status',
              send:'notSend',
              select: {
                type: 'api',
                api: {
                  apiPath: '/lead-statuses',
                  method: 'GET'
                },
                apiDataKey:'leads',
                apiValueKey: "id",
                apiViewValueKey: "status",
              },
              required: {
                value: true,
                errorMessage: 'Lead status is required.'
              },
              relatedTo: ['disposition'],
            },
            {
              name: 'disposition',
              type: 'select',
              placeholder: 'Disposition',
              select: {
                type: 'relative',
                api: {
                  apiPath: '/lead-statuses/${leadStatusId}',
                  method: 'GET',
                },
                apiDataKey:'leads[0].reasoning',
                apiViewValueKey: 'viewValue',
                apiValueKey: 'viewValue'
              },
              required: {
                value: true,
                errorMessage: 'Disposition is required.'
              },
              
            }, 
        
        
      ],
      submitApi: {
        apiPath: `/conversations`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Conversation created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.getLeadStatus();
    this.outputEvent.emit(event);
  }
  getLeadStatus(){
    if(this.leadStatusId){
      const leadStatus:CanApi = {
        apiPath:`/lead-statuses/${this.leadStatusId}`,
        method:'GET',
      }
      this.apiService.request(leadStatus).subscribe((res : any) =>{
        if(res){
         this.updateLead(res.status, this.leadStatusId)
        }
      })
    }
  }
  updateLead(leadStatus, leadStatusId){
    const submitApi:CanApi = {
      apiPath:`/leads/${this.leadId}`,
      method:'PATCH'
    }
   
    this.apiService.request(submitApi,{leadStatus, leadStatusId}).subscribe(res =>{
      if(res){
      this.updateService.isConversationAdded = true
      }
    })
  }
  beforeSubmitFormValue(conversation){
    console.log(conversation);
    if(conversation && conversation.leadStatusId){
      this.leadStatusId = conversation.leadStatusId
    }
    // const getFollowUps:CanApi = {
    //   apiPath:`/leadId`,
    //   method:'GET',
    //   params: new HttpParams().append('leadId',this.leadId.toString()).append('status','pending')
    // }
    // this.apiService.request(getFollowUps).subscribe((res:any[]) =>{
    //   if(res && res.length){
    //   }
    // })
  }
}
