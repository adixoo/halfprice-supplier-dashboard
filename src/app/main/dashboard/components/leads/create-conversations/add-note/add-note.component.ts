import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';
import { UpdateService } from '../../../orders/updates/update.service';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {

  constructor(
    private updateService : UpdateService,

  ) { }

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() leadId: string;
  formData: CanForm;
 

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      // sendAll:true,
      formFields: [     
        {
          name: 'notes',
          type: 'textarea',
          placeholder: 'Note',
          value:'notes',
          maxlength:'160'
        },
       
      ],
      getApi:{
        apiPath: `/leads/${this.leadId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/leads/${this.leadId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Note added successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.updateService.isConversationAdded = true
    this.outputEvent.emit(event);
  }
}
