import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-leads',
  templateUrl: './create-leads.component.html',
  styleUrls: ['./create-leads.component.scss']
})
export class CreateLeadsComponent implements OnInit {

  constructor() { }

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;


  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
      
        {
          name: 'firstName',
          type: 'text',
          placeholder: 'First Name',
          required: {
            value: true,
            errorMessage: 'First name is required.'
          }
        },
        {
          name: 'lastName',
          type: 'text',
          placeholder: 'Last Name',
        },
        {
          name: 'mobile',
          type: 'text',
          placeholder: 'Mobile',
          pattern:{
            value:'^[6-9]\\d{9}$',
            errorMessage:'Please enter valid mobile number'
          },
          required: {
            value: true,
            errorMessage: 'Mobile is required.'
          }
        },
        {
          name: 'altMobile',
          type: 'text',
          placeholder: 'Alternative Mobile',
          // pattern:{
          //   value:'^[6-9]\\d{9}$',
          //   errorMessage:'Please enter valid mobile number'
          // },
        },
        {
          name: 'email',
          type: 'text',
          placeholder: 'Email',
          pattern:{
            value:'^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$',
            errorMessage:'Please enter valid email'
          }
        },
        {
                  name: 'projectName',
                  type: 'select',
                  placeholder: 'Project',
                  select: {
                    type: 'api',
                    api: {
                      apiPath: '/projects',
                      method: 'GET',
                      params: new HttpParams()
                      .append('include', JSON.stringify([{ all: true }]))
                    },
                    apiValueKey: "name",
                    apiViewValueKey: "name"
                  },
                  relativeSetFields:[{
                    key:'builderName',
                    type:'different',
                    value :'builder.name',
                    differentType:'dataSource'
                  }]
              
                }, 
                {
                  name:'builderName',
                  type:'hidden',
                  value:'builder',
                  placeholder:null
                },
        {
          name: 'type',
          type: 'select',
          placeholder: 'Type',
          values: [ 
            { value: 'walkIn', viewValue: 'WalkIn' },
            { value: 'website', viewValue: 'Website' },
            { value: 'callback', viewValue: 'Callback' },
            { value: 'purchase', viewValue: 'Purchase' }
          ],
          required: {
            value: true,
            errorMessage: 'Lead status is required.'
          }
        }
      ],
      submitApi: {
        apiPath: `/leads`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Lead created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
