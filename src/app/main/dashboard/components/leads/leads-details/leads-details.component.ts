import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CanApiService } from 'src/@can/services/api/api.service';
import { ApiType, CanApi } from 'src/@can/types/api.type';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanButton } from 'src/@can/types/shared.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { UpdateService } from '../../orders/updates/update.service';
import { CommentsComponent } from '../comments/comments.component';
import { ConversationsComponent } from '../conversations/conversations.component';
import { EditLeadComponent } from '../edit-lead/edit-lead.component';
import { CreateFollowUpsComponent } from '../follow-ups/create-follow-ups/create-follow-ups.component';
import { FollowUpsComponent } from '../follow-ups/follow-ups.component';
import { LeadOrdersComponent } from '../lead-orders/lead-orders.component';
import { UpdateLeadStatusComponent } from '../update-lead-status/update-lead-status.component';

@Component({
  selector: 'app-leads-details',
  templateUrl: './leads-details.component.html',
  styleUrls: ['./leads-details.component.scss']
})
export class LeadsDetailsComponent implements OnInit {

  public leadId:number;  
  public detailViewData: CanDetailView;
  public fabButtonConfig: CanFabButton;
  public postUpdatesButtonConfig : CanButton;
  public tabViewConfig: CanTabView;
  private subscription:Subscription;
  private user:any;
  
  refreshEvent = new EventEmitter<boolean>(true);


  constructor(
    private updateService : UpdateService,
    private activedRoute: ActivatedRoute,   
    private apiService: CanApiService,
   ) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => this.leadId = parseInt(paramMap.get('id')));
  }

 async ngOnInit() {
   this.user =  await this.getUserDetails();
    this.tabViewConfig = {
      lazyLoading: true,
      tabs: []
    };
    this.tabViewConfig.tabs.push({
      component: ConversationsComponent,
      label: 'Conversations',
      inputData: [{
        key:'leadId',
        value:this.leadId
      }]
    })
    this.tabViewConfig.tabs.push({
      component: FollowUpsComponent,
      label: 'Follow Ups',
      inputData: [{
        key:'leadId',
        value:this.leadId
      }]
    })
    if(this.user){
      this.tabViewConfig.tabs.push({
        component: LeadOrdersComponent,
        label: 'Orders',
        inputData: [{
          key:'userId',
          value:this.user.id
        }]
      })
    }
    this.subscription= this.updateService.isFollowUpsAdded$.subscribe(value => {
      this.refreshEvent.next(value)
    })

    this.updateService.isConversationAdded$.subscribe(value => {
      this.refreshEvent.next(value)
    })
    this.postUpdatesButtonConfig = {
      label : 'Create FollowUp',
      type : 'raised',
      color : 'primary',
      float:'left'
    }  
   
        // // Fab Button Config
        // this.fabButtonConfig = {
        //   icon: {
        //     name: 'message',
        //     tooltip: 'Comments'
        //   },
        //   type: 'modal',
        //   position: CanFabButtonPosition.BottomRight,
        //   modal: {
        //     component: CommentsComponent,
        //     inputData: [{
        //       inputKey:'leadId',
        //       type:'fixed',
        //       value:this.leadId
        //     }],
        //     width: 600,
        //     header: null
        //   },
        // }
  }

  getUserDetails(){
    return new Promise((resolve, reject) =>{
      const getLead:CanApi = {
        apiPath:`/leads/${this.leadId}`,
        method:'GET',
      }
      this.apiService.request(getLead).subscribe((lead:any) =>{
        const getUser: CanApi = {
          apiPath : '/users',
          method : 'GET',
          params: new HttpParams().append('mobile',lead.mobile.toString()),
          apiType:ApiType.Loopback
        }
        this.apiService.request(getUser).subscribe((user) =>{
         resolve(user[0]);
        })
      })
    })
  }
  ngOnDestroy(){
    if(this.subscription && !this.subscription.closed){
      this.subscription.unsubscribe();
    }
  }

}
