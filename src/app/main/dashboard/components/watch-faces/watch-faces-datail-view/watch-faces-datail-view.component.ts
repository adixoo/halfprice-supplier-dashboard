import { Component, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanIconType } from 'src/@can/types/shared.type';

@Component({
  selector: 'app-watch-faces-datail-view',
  templateUrl: './watch-faces-datail-view.component.html',
  styleUrls: ['./watch-faces-datail-view.component.scss']
})
export class WatchFacesDatailViewComponent implements OnInit {

  constructor() { }
  public detailViewData: CanDetailView;
  dataSource : any

  ngOnInit() {
     // Detail View Config Init
     this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 1,
      dataSource:this.dataSource,
      labelPosition: 'inline',
      displayedFields: [
        {
          header:'Name',
          type:'text',
          value:'name'
        },
        {
          header: 'Mockup',
          type: 'image',
          images: {
            showAll: true,
            openType: 'modal',
            imageItems: [
              {
                type: 'api',
                isArray: false,
                alt: 'Preview',
                value: 'imageUrl'
              }
            ]
          }
        },
        {
          header: 'Binary',
          type: 'document',
          documents: {
            showAll: true,
            openType: 'modal',
        documentItems:[{
          type:'api',
          value:'fileUrl',
          // openType:'modal'
        }]
          }
        },
        {
          header:'type',
          type:'text',
          value:'type'
        },
        {
          header:'Dial Number',
          type:'text',
          value:'dialNumber'
        },
        {
          header:'Is Editable',
          type:'text',
          value:'isEditable'
        },
       
        {
          header: 'Status',
          type: 'enum_icon',
          value: 'status',
          enumIcons: [
            { value: 'active',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Active', color:  '#10d817'} },
            { value: 'inactive',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Inactive', color:  '#ee3f37'} },

          ]
        },
        {
          header : 'Product',
          type : 'text',
          value : 'product.name'
        },
        {
          header : 'Created At',
          type : 'date',
          value : 'createdAt',
          dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
        },
    
        {
          header : 'Updated At',
          type : 'date',
          value : 'updatedAt',
          dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
        },

      ],
 
      header: "Watch Faces Details"
    }
  }

}
