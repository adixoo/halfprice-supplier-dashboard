import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateWatchFacesComponent } from './create-watch-faces/create-watch-faces.component';
import { EditWatchFacesComponent } from './edit-watch-faces/edit-watch-faces.component';
import { WatchFacesDatailViewComponent } from './watch-faces-datail-view/watch-faces-datail-view.component';

@Component({
  selector: 'app-watch-faces',
  templateUrl: './watch-faces.component.html',
  styleUrls: ['./watch-faces.component.scss']
})
export class WatchFacesComponent implements OnInit {

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;
  public products:any =[]
 
  constructor(private apiService : CanApiService) {
    // Fetching Path Params
   }

 async ngOnInit() {
    this.products = await this.getProducts()
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [

          {
            header : 'id',
            type : 'text',
            value:'id'
          },
          {
            header: 'Watch Face',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'imageUrl'
                }
              ]
            }
          },
          {
            header:'Name',
            type:'text',
            value:'name'
          },
          {
            header : 'Watch Name',
            type : 'text',
            value : 'product.name'
          },
         
          {
            header:'type',
            type:'text',
            value:'type'
          },
          {
            header:'Dial Number',
            type:'text',
            value:'dialNumber'
          },
          {
            header: 'Status',
            type: 'enum_icon',
            value: 'status',
            enumIcons: [
              { value: 'active',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Active', color:  '#10d817'} },
              { value: 'inactive',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Inactive', color:  '#ee3f37'} },
  
            ]
          },
         
          {
            header : 'Created At',
            type : 'date',
            value : 'createdAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          },
      
          {
            header : 'Updated At',
            type : 'date',
            value : 'updatedAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          },

        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'ajax',
              api: {
                apiPath: '/watch-faces/${id}',
                method: 'PATCH'
              },
              bodyParams: [
                {
                  key: 'status',
                  value: 'active'
                },
              ],
              confirm: {
                title: 'Change Status',
                message: 'Are you sure you want to active watch face?',
                buttonText: {
                  confirm: 'Confirm',
                  cancel: 'Cancel'
                }
              },
              displayCondition: {
                type: 'single',
                match: { operator: 'equals', key: 'status', value: 'inactive' }
              }
            },
            icon: { 
              type: CanIconType.Material, 
              name: 'thumb_up_al', 
              tooltip: 'change status', 
              color:  '#10d817'
            }
          },
          {
            action: {
              actionType: 'ajax',
              api: {
                apiPath: '/watch-faces/${id}',
                method: 'PATCH'
              },
              bodyParams: [
                {
                  key: 'status',
                  value: 'inactive'
                },
              ],
              confirm: {
                title: 'Change Status',
                message: 'Are you sure you want to inactive watch face?',
                buttonText: {
                  confirm: 'Confirm',
                  cancel: 'Cancel'
                }
              },
              displayCondition: {
                type: 'single',
                match: { operator: 'equals', key: 'status', value: 'active' }
              }
            },       
            icon: { 
              // name: 'edit', 
              // tooltip: 'change status', 
              type: CanIconType.Material, 
              name: 'thumb_down_al', 
              tooltip: 'change status', 
              color:  '#ee3f37'
            } 
          },
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditWatchFacesComponent,
                inputData: [
                  {
                    inputKey: 'watchFaceId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Update Watch Face',
                width: 600
              },  
            },
            icon: {
              name: 'edit',
              tooltip: 'Update Watch Face',
            }
          },
          {
            action: {
              actionType: 'modal',
              modal: {
                component: WatchFacesDatailViewComponent,
                inputData: [
                ],
                // header: 'Product Details',
                width: 350
              }
            },
            icon: {
              type: CanIconType.Flaticon,
              name: 'flaticon-eye',
              tooltip: 'View'
            }
          },
       
        ],
        filters: [
          // {
          //   filtertype: 'local',
          //   placeholder: 'Search',
          //   keys: ['name','internalSku','externalSku']
          // },
          // {
          //   filtertype: 'api',
          //   placeholder: 'Search',
          //   type: 'text',
          //   key: 'id',
          //   searchType: 'autocomplete',
          //   autoComplete: {
          //     type: 'api',
          //     apiValueKey: 'id',
          //     apiViewValueKey: 'name',
          //     autocompleteParamKeys: ['name','internalSku','externalSku'],
          //     api: {
          //       apiPath: '/products',
          //       method: 'GET',
          //       params : new HttpParams()
          //       .set('masterProductId',this.masterProductId)
          //     }
          //   },
          // },
          {
            filtertype: "api",
            placeholder: "Status",
            type: "dropdown",
            key: "status",
            value: [
              { value: "active", viewValue: "Active" },
              { value: "inactive", viewValue: "Inactive" }
            ]
          },
          {
            filtertype: 'api',
            placeholder: 'Products',
            type: 'dropdown',
            key: 'productId',
            // multipleSelect:true,
            value:this.products
          },
          {
            filtertype: "api",
            placeholder: "Type",
            type: "dropdown",
            key: "type",
            value: [
              { value: 'static', viewValue: 'Static' },
              { value: 'cloud', viewValue: 'Cloud' },
              { value: 'custom', viewValue: 'Custom' },
            ]
          },
        ],
        api: {
          apiPath: '/watch-faces',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{ all: true }])),
        },
        apiDataKey: 'data',
        countApi: {
          apiPath: '/watch-faces/count',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{  all: true  }]))
        },
        countApiDataKey: 'data.count',
        pagination: {
          pageSizeOptions: [50, 100]
        },
        header: 'Watch faces'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateWatchFacesComponent,
        inputData: [],
        width: 600,
        header: 'Add Watch Faces'
      }
    }
  }

  getProducts(){
    return new Promise((resolve, reject) =>{
       const getApi:CanApi = {
         apiPath:`/products`,
         method:'GET',
        //  params:new HttpParams().append('status','active')
       }
       this.apiService.request(getApi).subscribe((product: any) =>{
         const products = []
         for (let index = 0; index < product.data.length; index++) {
           const productData = product.data[index]
           products.push({
             value:productData['id'],
             viewValue:productData['name'].toUpperCase()
           })     
         }
         resolve(products)
       }) 
   })
 }
}
