import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatchFacesComponent } from './watch-faces.component';

describe('WatchFacesComponent', () => {
  let component: WatchFacesComponent;
  let fixture: ComponentFixture<WatchFacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatchFacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchFacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
