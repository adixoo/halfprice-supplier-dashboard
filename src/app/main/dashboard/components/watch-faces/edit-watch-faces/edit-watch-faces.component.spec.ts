import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditWatchFacesComponent } from './edit-watch-faces.component';

describe('EditWatchFacesComponent', () => {
  let component: EditWatchFacesComponent;
  let fixture: ComponentFixture<EditWatchFacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditWatchFacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditWatchFacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
