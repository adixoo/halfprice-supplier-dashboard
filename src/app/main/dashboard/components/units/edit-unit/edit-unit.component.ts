import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-unit',
  templateUrl: './edit-unit.component.html',
  styleUrls: ['./edit-unit.component.scss']
})
export class EditUnitComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() unitId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [        
        // {
        //   name: 'name',
        //   type: 'text',
        //   placeholder: 'Name',
        //   value:'name',
        //   required: {
        //     value: true,
        //     errorMessage: 'Name is required.'
        //   }
        // },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ]
        }
      ],
      getApi:{
        apiPath: `/units/${this.unitId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/units/${this.unitId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Builder updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
