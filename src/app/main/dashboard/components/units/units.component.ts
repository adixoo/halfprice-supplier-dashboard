import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateUnitComponent } from './create-unit/create-unit.component';
import { EditUnitComponent } from './edit-unit/edit-unit.component';
import { UnitDetailsComponent } from './unit-details/unit-details.component';

@Component({
  selector: 'app-unit',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.scss']
})
export class UnitsComponent implements OnInit {

  constructor() { }

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;
  @Input() projectId : any;
 
  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Description',
            type: 'text',
            value: 'description',
          },
          {
            header:"Project Name",
            type :'text',
            value :'project.name'
          },
          {
            header:"Builder Name",
            type :'text',
            value:"builder.name"
          },
          {
            header:'Tower',
            type :'text',
            value :"tower.name"
          },
          {
            header:'Status',
            type:'text',
            value:'status'
          },
          {
            header : 'Created At',
            type : 'date',
            value : 'createdAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          },
          {
            header : 'Updated At',
            type : 'date',
            value : 'updatedAt',
            dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
          }
          // {
          //   header:'Logo',
          //   type:'image',
          //   value:'logo'
          // }
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditUnitComponent,
                inputData: [
                  {
                    inputKey: 'unitId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Change Status',
                width: 600
              }
            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Unit',
            }
          },

          {
            action: {
              actionType: 'modal',
              modal: {
                component: UnitDetailsComponent,
                inputData: [
                  {
                    inputKey: 'unitId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: "Unit Details",
                width: 600
              }
            },
            icon: {
              type: CanIconType.Flaticon,
              name: 'flaticon-eye',
              tooltip: 'View'
            }
          }
        ],
        filters: [
          {
            filtertype: 'local',
            placeholder: 'Search',
            keys: ['name','tower.name']
          }
        ],
        api: {
          apiPath: '/units',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{ all: true }]))
          .set('projectId',this.projectId)
        },
        countApi: {
          apiPath: '/units/count',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{ all: true }]))
          .set('projectId',this.projectId)

  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100, 200]
        },
        header: 'Units'
      }

      // Fab Button Config
    // this.fabButtonConfig = {
    //   icon: {
    //     name: 'add',
    //     tooltip: 'Create New'
    //   },
    //   type: 'modal',
    //   position: CanFabButtonPosition.BottomRight,
    //   modal: {
    //     component: CreateUnitComponent,
    //     inputData: [],
    //     width: 600,
    //     header: 'Add Unit'
    //   },
    // }
  }


}
