import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ApiType } from 'src/@can/types/api.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateConfigurationComponent } from './create-configuration/create-configuration.component';
import { CreateProjectConfigurationComponent } from './create-project-configuration/create-project-configuration.component';
import { EditConfigurationComponent } from './edit-configuration/edit-configuration.component';

@Component({
  selector: 'app-configurations',
  templateUrl: './configurations.component.html',
  styleUrls: ['./configurations.component.scss']
})
export class ConfigurationsComponent implements OnInit {

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;
 @Input() projectId;any;
  constructor() { }

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'name',
            type: 'text',
            value: 'name',
          },
          // {
          //   header: 'Project Name',
          //   type: 'text',
          //   value: 'project.name',
          // },
          {
            header: 'description',
            type: 'text',
            value: 'description',
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          },
          // {
          //   header: 'image',
          //   type: 'image',
          //   images: {
          //     showAll: true,
          //     openType: 'modal',
          //     imageItems: [
          //       {
          //         type: 'api',
          //         isArray: false,
          //         alt: 'Preview',
          //         value: 'image'
          //       }
          //     ]
          //   }
          // },
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditConfigurationComponent,
                inputData: [
                  {
                    inputKey: 'configurationId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Configuration',
                width: 600
              },
      permission: { type: 'single', match: { key: 'CREATE_CONFIGURATIONS', value: true }}

            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Configuration',
            }
          }
        ],
        filters: [
          {
            filtertype: 'local',
            placeholder: 'Search',
            keys: ['name']
          },
          {
            filtertype: 'api',
            type: 'download',
            placeholder: 'Download',
            download: {
              downloadType: "api",
              extension: ".xlsx",
              api: {
                apiPath: "/configurations",
                apiType: ApiType.Loopback,
                responseType: "blob",
                method: "GET",
                params: new HttpParams()  
                  .append('exportExcel', JSON.stringify({ headerDisplayType: 'propercase' , "exportAll":true})),
              },
            }
          }
        ],
        api: {
          apiPath: '/configurations',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{ all: true }]))
          .append('projectId',this.projectId)

        },
        countApi: {
          apiPath: '/configurations/count',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100 , 200]
        },
        header: 'Configurations'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateProjectConfigurationComponent,
        inputData: [{
          inputKey:'projectId',
          type:'fixed',
          value:this.projectId
        }],
        width: 600,
        header: 'Add Configuration'
      },
      permission: { type: 'single', match: { key: 'CREATE_CONFIGURATIONS', value: true }}

    }
      // if(this.projectId){
      //   this.tableConfig.api.params = new HttpParams()
      //   .append('include', JSON.stringify([{ all: true }]))
      //   .append('projectId',this.projectId)

      //   this.fabButtonConfig.modal.component = CreateProjectConfigurationComponent
          
      // }
  }

}
