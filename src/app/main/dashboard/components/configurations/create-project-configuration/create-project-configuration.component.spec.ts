import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProjectConfigurationComponent } from './create-project-configuration.component';

describe('CreateProjectConfigurationComponent', () => {
  let component: CreateProjectConfigurationComponent;
  let fixture: ComponentFixture<CreateProjectConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProjectConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProjectConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
