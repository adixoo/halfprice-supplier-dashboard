import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  constructor() { }
  public tableConfig: CanTable;

  ngOnInit() {
 
    // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Name',
          type: 'text',
          value: 'firstName middleName lastName',
          keySeparators:[' ']
        },
        {
          header: 'Phone',
          type: 'text',
          value: 'mobile',
        },
        {
          header: 'Email',
          type: 'text',
          value: 'email',
        },

  
        // {
        //   header: 'Role',
        //   type: 'text',
        //   value: 'role',
        // },
        {
          header: 'Created At',
          type: 'date',
          value: 'createdAt',
          dateDisplayType: 'dd/MM/yy'
        },
      ],
      filters: [
        {
          filtertype: 'api',
          placeholder: 'Search',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'name',
            autocompleteParamKeys: ['firstName', 'lastName', 'name', 'mobile'],
            api: {
              apiPath: '/users',
              method: 'GET',
              params : new HttpParams().set('type', 'customer')
            },
            apiDataKey:'data'

          },
        
        },
        {
          filtertype: 'api',
          placeholder: 'Status',
          type: 'dropdown',
          key: 'status',
          // selectedValue: 'new',
          value: [
            { value: 'active', viewValue: 'Active' },
            { value: 'inactive', viewValue: 'Inactive' },

          ]
        },
        {
          filtertype: 'api',
          placeholder: 'Created At',
          type: 'date',
          key: 'createdAt',
          date: {
            enabledRange: true,
            enabledTime: false,
            dateRange:{
              maxDate : new Date()
            }
          }
        },
      ],
      fieldActions: [
        {
          action: {
            actionType: 'link',
            link: {
              url: '/dashboard/user-detail/${id}',
              target: 'self',
              type: 'url'
            },
        permission: { type: 'single', match: { key: 'READ_USER', value: true } },
            
          },
          icon: {
            type: CanIconType.Material,
            name: 'note_add',
            tooltip: 'Customer Detail',
            color: 'blue'
          }
        },
      ],
      api: {
        apiPath: '/users',
        method: 'GET',
        params : new HttpParams().set('type', 'customer')
      },
      apiDataKey: 'data',
      countApi: {
        apiPath: '/users/count',
        method: 'GET',
        params : new HttpParams().set('type', 'customer')

      },
      countApiDataKey: 'data.count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: 'Consumers'
    }
  }

}
