import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { DeviceComponent } from './device/device.component';
import { EmergencyContactsComponent } from './emergency-contacts/emergency-contacts.component';
import { WearableComponent } from './wearable/wearable.component';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent implements OnInit {

  public userId:number;
  public user:any;
  public devices:any[] = [];
  public wearables:any[] = [];
  public tabViewConfig: CanTabView;
  constructor(
    private activedRoute: ActivatedRoute,
    private apiService: CanApiService
  ) {
        // Fetching Path Params
        this.activedRoute.paramMap.subscribe(paramMap => this.userId = parseInt(paramMap.get('id')));
   }

 async ngOnInit() {
  await this.getUser();
  await this.getUserProduct();
    this.tabViewConfig = {
      lazyLoading: true,
      loadsEveryTime:false,
      tabs: []
    };
    this.tabViewConfig.tabs.push({
      component: DeviceComponent,
      label: 'Devices',
      inputData: [{
        key: 'devices',
        value: this.devices
      },
      {
        key: 'userId',
        value: this.userId
      }]
    });
    this.tabViewConfig.tabs.push({
      component: WearableComponent,
      label: 'Wearable',
      inputData: [{
        key: 'wearables',
        value: this.wearables
      }]
    })
    this.tabViewConfig.tabs.push({
      component: EmergencyContactsComponent,
      label: 'Emergency Contacts',
      inputData: [{
        key: 'userId',
        value: this.userId
      }]
    })
    
   
  }

  getUserProduct(){
    return new Promise((resolve, reject) => {
      const getProperties: CanApi = {
        apiPath: `/user-products`,
        method: "GET",
        params: new HttpParams()
        .append('userId', this.userId.toString())
        .append('include', JSON.stringify([{ all: true }]))
      };
      this.apiService.request(getProperties).subscribe((res:any) => {
        for (let index = 0; index < res.data.length; index++) {
          if(res.data[index]['deviceId']){ 
              this.devices.push(res.data[index]['device'])          
          }
          if(res.data[index]['wearableId']){
            this.wearables.push(res.data[index]['wearable'])
          }
        }
        resolve(res);
      });
    });
  }

  getUser(){
    return new Promise((resolve, reject) => {
      const getProperties: CanApi = {
        apiPath: `/users/${this.userId}`,
        method: "GET",
        params: new HttpParams()
        .append('include', JSON.stringify([{ all: true }]))
      };
      this.apiService.request(getProperties).subscribe((res:any) => {
        this.user = res.data;
        resolve(res);
      });
    });
  }
}
