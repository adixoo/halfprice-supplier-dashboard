import { Component, Input, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';

@Component({
  selector: 'app-basic-info-card',
  templateUrl: './basic-info-card.component.html',
  styleUrls: ['./basic-info-card.component.scss']
})
export class BasicInfoCardComponent implements OnInit {

  constructor() { }
  @Input() userId:any;
  public detailViewData: CanDetailView;


  ngOnInit() {
       // Detail View Config Init
       this.detailViewData = {
        discriminator: 'detailViewConfig',
        columnPerRow: 1,
        labelPosition: 'inline',
        // dataSource:this.user,
        displayedFields: [
          {
            header: 'Mobile',
            type: 'text',
            value: 'mobile'
          },
          {
            header: 'Email',
            type: 'text',
            value: 'email',
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          }
        ],
      api:{
        apiPath: `/users/${this.userId}`,
        method:'GET'
      },
      apiDataKey: 'data'

      }
  }

}
