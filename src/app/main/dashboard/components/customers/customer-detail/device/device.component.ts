import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {

  
  @Input() devices : any;
  @Input() userId : number;
  public tableConfig: CanTable;
  constructor() { }

  ngOnInit() {
    this.tableConfig = {
      discriminator: 'tableConfig',
      // dataSource:this.devices,
      displayedColumns: [
   {
     type:"text",
     header:"Device Id",
     value:'deviceId'
   },
   {
     header:"Platform",
     type:'text',
     value:'platform'
   },
   {
    header:"OS Version",
    type:'text',
    value:'osVersion'
  },
  {
    header:"Brand",
    type:'text',
    value:'brand'
  },
  {
    header:"Manufacturer",
    type:'text',
    value:'manufacturer'
  },
  {
    header:"Model",
    type:'text',
    value:'model'
  },
  {
    header:"Screen Resolution",
    type:'text',
    value:'screenResolution'
  },
  {
    header:"RAM",
    type:'text',
    value:'ram'
  },
  {
    header:"App Version",
    type:'text',
    value:'appVersion'
  },
  {
    header:"Serial Number",
    type:'text',
    value:'serialNo'
  },
  {
    header: 'Status',
    type: 'enum_icon',
    value: 'status',
    enumIcons: [
      { value: 'active',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Active', color:  '#10d817'} },
      { value: 'inactive',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Inactive', color:  '#ee3f37'} },

    ]
  }
      ],
      
      api: {
        apiPath: '/devices',
        method: 'GET',
        params : new HttpParams()
        .append('userId', this.userId.toString())
      },
      apiDataKey: 'data',
      countApi: {
        apiPath: '/devices/count',
        method: 'GET',
      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      // header: 'devices'
    }
  }

}
