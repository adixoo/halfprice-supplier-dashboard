import { Component, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';

@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.scss']
})
export class BasicDetailsComponent implements OnInit {
  public detailViewData: CanDetailView;

  constructor() { }

  ngOnInit() {
    this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 5,
      labelPosition: 'top',
      displayedFields: [
        {
          header: 'Name',
          type: 'text',
          value: 'name',
        },

        {
          header: 'Status',
          type: 'text',
          value: 'status',
        },
      ],
      api: {
        apiPath: ``,
        method: 'GET'
      },
      header: ''
    }
  }

}
