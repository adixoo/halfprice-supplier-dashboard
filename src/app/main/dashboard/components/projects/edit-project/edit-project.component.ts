import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanAuthService } from 'src/@can/services/auth/auth.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanFormStepper, CanFormStepperLayout } from 'src/@can/types/form-stepper.type';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss']
})
export class EditProjectComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() projectId: any;
  @Input() addressId: number;
  public formStepperConfig: CanFormStepper;
  dataSource : any;
  projectForm: CanForm;
  addressForm: CanForm;
  createAddressForm: CanForm;


  constructor(
    private apiService: CanApiService,
    private authService: CanAuthService ,
  ) { }

  ngOnInit() {
  

    this.projectForm = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      dataSource:this.dataSource,
      formFields: [
        {
             name: 'name',
             type: 'text',
             placeholder: 'Name',
             value:'name',
             required: {
               value: true,
               errorMessage: 'Name is required.'
             }
         },
         {
           name: 'status',
           type: 'select',
           placeholder: 'Status',
           value:'status',
           values: [
             { value: 'active', viewValue: 'active' },
             { value: 'inactive', viewValue: 'inactive' },
           ],
           required: {
             value: true,
             errorMessage: 'Status is required.'
           }
         }
             
         ],
      // sendAll:true,
      
      // getApi:{
      //   apiPath: `/leads/${this.leadId}`,
      //   method:'GET'
      // },
      submitApi: {
        apiPath: `/projects/${this.projectId}`,
        method: 'PATCH'
      },

      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Project Updated successfully!"
    }


    this.addressForm = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      // dataSource:this.dataSource,
    formFields: [
      {
        name: 'stateId',
        type: 'autocomplete',
        placeholder: 'State',
        value:'stateId',
        required: {
          value: true,
          errorMessage: 'State is required'
        },
        autoComplete: {
          type: 'api',
          api: {
            apiPath: '/states',
            method: 'GET',
          },
          autocompleteParamKeys: ['name'],
          apiValueKey: 'id',
          apiViewValueKey: 'name',
        }
      },
      {
        name: 'cityId',
        type: 'autocomplete',
        placeholder: 'City',
        value:'cityId',
        required: {
          value: true,
          errorMessage: 'City is required'
        },
        autoComplete: {
          type: 'api',
          api: {
            apiPath: '/cities',
            method: 'GET',
          },
          params: [{
            key: 'stateId',
            formValue: 'stateId'
          }],
          autocompleteParamKeys: ['name'],
          apiValueKey: 'id',
          apiViewValueKey: 'name',
        }
      },
              // {
              //   name: 'state',
              //   type: 'autocomplete',
              //   placeholder: 'State',
              //   value: 'state',
              //   autoComplete: {
              //     type: 'api',
              //     api: {
              //       apiPath: '/states',
              //       method: 'GET'
              //     },
              //     autocompleteParamKeys: ['name'],
              //     apiValueKey: 'name',
              //     apiViewValueKey: 'name',
                  
              //   },
              //   relatedTo:['city'],
              //   // relativeSetFields: [{
              //   //   key: 'stateId',
              //   //   type: 'different',
              //   //   // differentType: "",
              //   //   value: 'id'
              //   // }],
              //   required: {
              //     value: true,
              //     errorMessage: 'State is required.'
              //   }
              // },
              // {
              //   name: 'city',
              //   type: 'autocomplete',
              //   placeholder: 'City',
              //   value: 'city',
              //   autoComplete: {
              //     type: 'api',
              //     api: {
              //       apiPath: '/cities',
              //       method: 'GET'
              //     },
              //     params: [{
              //       key: 'stateId',
              //       formValue: 'stateId'
              //     }],
              //     autocompleteParamKeys: ['name'],
              //     apiValueKey: 'name',
              //     apiViewValueKey: 'name',
              //   },
              //   required: {
              //     value: true,
              //     errorMessage: 'City is required.'
              //   }
              // },
              
              {
                name: 'address',
                type: 'text',
                placeholder: 'Address',
                value:'address',
                required: {
                  value: true,
                  errorMessage: 'Address is required.'
                }
              },
             
            ],
      // sendAll:true,
      
      getApi:{
        apiPath: `/addresses/${this.addressId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/addresses/${this.addressId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Addresses Updated successfully!"
    }

    this.createAddressForm = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      sendAll:true,
      formFields: [
        
        {
          name: 'stateId',
          type: 'autocomplete',
          placeholder: 'State',
          required: {
            value: true,
            errorMessage: 'State is required'
          },
          autoComplete: {
            type: 'api',
            api: {
              apiPath: '/states',
              method: 'GET',
            },
            autocompleteParamKeys: ['name'],
            apiValueKey: 'id',
            apiViewValueKey: 'name',
          }
        },
        {
          name: 'cityId',
          type: 'autocomplete',
          placeholder: 'City',
          required: {
            value: true,
            errorMessage: 'City is required'
          },
          autoComplete: {
            type: 'api',
            api: {
              apiPath: '/cities',
              method: 'GET',
            },
            params: [{
              key: 'stateId',
              formValue: 'stateId'
            }],
            autocompleteParamKeys: ['name'],
            apiValueKey: 'id',
            apiViewValueKey: 'name',
          }
        },
        {
          name:'pinCode',
          placeholder:'Pin code',
          type:'text',
          required: {
            value: true,
            errorMessage: 'Pin Code is required.'
          }
        },
        {
          name:'userId',
          type:'hidden',
          placeholder:null,
          defaultValue:parseInt(this.authService.currentUser()['id'])
        },
        {
          name: 'address',
          type: 'text',
          placeholder: 'Address',
          value:'address',
          required: {
            value: true,
            errorMessage: 'Address is required.'
          }
        },
       
      ],
      submitApi: {
        apiPath: `/addresses`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Adderess created successfully!"
    }
    // this.formStepperConfig = {
    //   discriminator: 'formStepperConfig',
    //   type: 'edit',
    //   dataSource:this.dataSource,
    //   layout: CanFormStepperLayout.Vertical,
    //   submitOnEachStep: true,
    //   apiIdKey: 'id',
    //   backButton: {
    //     type: 'basic',
    //     color: 'primary',
    //     label: 'Back'
    //   },
     
    //   submitApi: {
    //     apiPath: `/addresses/${this.addressId}`,
    //     method: 'PATCH'
    //   },
    //    firstTimeSubmitApi :{       
    //     apiPath: `/projects/${this.projectId}`,
    //     method: 'PATCH'
    //   },

    //   // getApi: {
    //   //   apiPath: `/projects/${this.projectId}`,
    //   //   method: 'GET'
    //   // },
    //   steps: [
    //     {
    //       label: 'Project Details',
    //       form: {
    //         type: 'edit',
    //         discriminator: 'formConfig',
    //         columnPerRow: 1,
    //         sendAll: true,
    //         formFields: [
    //        {
    //             name: 'name',
    //             type: 'text',
    //             placeholder: 'Name',
    //             value:'name',
    //             required: {
    //               value: true,
    //               errorMessage: 'Name is required.'
    //             }
    //         },
    //         {
    //           name: 'status',
    //           type: 'select',
    //           placeholder: 'Status',
    //           value:'status',
    //           values: [
    //             { value: 'active', viewValue: 'active' },
    //             { value: 'inactive', viewValue: 'inactive' },
    //           ],
    //           required: {
    //             value: true,
    //             errorMessage: 'Status is required.'
    //           }
    //         }
                
    //         ],
    //         formButton:
    //         {
    //           type: 'raised',
    //           color: 'primary',
    //           label: 'Next'
    //         },
    //       }
    //     },
    //     {
    //       label: 'Address Details',
    //       form: {
    //         type: 'edit',
    //         discriminator: 'formConfig',
    //         columnPerRow: 1,
    //         sendAll: true,
    //         formFields: [
        
    //           {
    //             name: 'state',
    //             type: 'autocomplete',
    //             placeholder: 'State',
    //             value: 'address.state',
    //             autoComplete: {
    //               type: 'api',
    //               api: {
    //                 apiPath: '/states',
    //                 method: 'GET'
    //               },
    //               autocompleteParamKeys: ['name'],
    //               apiValueKey: 'name',
    //               apiViewValueKey: 'name',
    //             },
    //                required: {
    //               value: true,
    //               errorMessage: 'State is required.'
    //             }
    //           },
    //           {
    //             name: 'city',
    //             type: 'autocomplete',
    //             placeholder: 'City',
    //             value: 'address.city',
    //             autoComplete: {
    //               type: 'api',
    //               api: {
    //                 apiPath: '/cities',
    //                 method: 'GET'
    //               },
    //               autocompleteParamKeys: ['name'],
    //               apiValueKey: 'name',
    //               apiViewValueKey: 'name',
    //             },
    //                required: {
    //               value: true,
    //               errorMessage: 'City is required.'
    //             }
    //           },
              
    //           {
    //             name: 'address',
    //             type: 'text',
    //             placeholder: 'Address',
    //             value:'address.address',
    //             required: {
    //               value: true,
    //               errorMessage: 'Address is required.'
    //             }
    //           },
             
    //         ],
    //         formButton:
    //         {
    //           type: 'raised',
    //           color: 'primary',
    //           label: 'Save'
    //         },
    //       }
    //     }   
       
    //   ]
    // }
    
  }

  isAddress(id){
    return typeof id =='string' ? false : true
  }

  updateProject(adderess){
    if(adderess){
      const submitApi:CanApi = {
        apiPath:`/projects/${this.projectId}`,
        method:'PATCH'
      }
      const bodyData ={
        addressId : adderess.id
      }
      this.apiService.request(submitApi,bodyData).subscribe(res =>{
        if(res){
        
        }
      })
    }
  }
  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
