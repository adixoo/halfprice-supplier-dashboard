import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateProjectComponent } from './create-project/create-project.component';
import { EditProjectComponent } from './edit-project/edit-project.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';

@Component({
  selector: 'app-project',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor() { }
  public tableConfig: CanTable;
  @Input() builderId : any; 
  public fabButtonConfig: CanFabButton;

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          },
          {
            header :"Address",
            type : 'text',
            value:'address.address'
          },

          {
            header :"City",
            type : 'text',
            value:'address.city'
          },

          {
            header :"State",
            type : 'text',
            value:'address.state'
          }
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditProjectComponent,
                inputData: [
                  {
                    inputKey: 'projectId',
                    type: 'key',
                    key: 'id'
                  },
                  {
                    inputKey: 'addressId',
                    type: 'key',
                    key: 'addressId'
                  }
                ],
                header: 'Edit Project',
                width: 600
              },
      permission: { type: 'single', match: { key: 'UPDATE_PROJECTS', value: true }}

            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Project',
            }
          },
          {
            // action: {
            //   actionType: 'modal',
            //   modal: {
            //     component: ProjectDetailsComponent,
            //     inputData: [
            //       {
            //         inputKey: 'projectId',
            //         type: 'key',
            //         key: 'id'
            //       }
            //     ],
            //     header: 'Project Details',
            //     width: 600
            //   }
            // },
            action: {
              actionType: 'link',
              link: {
                url: '/dashboard/project/${id}',
                target: 'self',
                type: 'url'
              }
            },
            icon: {
              type: CanIconType.Flaticon,
              name: 'flaticon-eye',
              tooltip: 'View'
            }
          }
        ],
        filters: [
          // {
          //   filtertype: 'local',
          //   placeholder: 'Search',
          //   keys: ['name']
          // },
          {
            filtertype: 'api',
            placeholder: 'Search',
            type: 'text',
            key: 'id',
            searchType: 'autocomplete',
            autoComplete: {
              type: 'api',
              apiValueKey: 'id',
              apiViewValueKey: 'name',
              autocompleteParamKeys: ['name'],
              api: {
                apiPath: '/projects',
                method: 'GET',
                params : new HttpParams()
                .set('builderId',this.builderId)
              }
            },
          },
          {
            filtertype : 'api',
            placeholder:'status',
            type: "dropdown",
            key: "status",
            value: [
              { value: "active", viewValue: "Active" },
              { value: "inactive", viewValue: "Inactive" }
            ]
          }
        ],
        api: {
          apiPath: '/projects',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{ all: true }]))
          .set('builderId',this.builderId)

        },
        countApi: {
          apiPath: '/projects/count',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{ all: true }]))
          .set('builderId',this.builderId)

  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100, 200]
        },
        header: 'Projects'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateProjectComponent,
        inputData: [{
          
            inputKey: "builderId",
            type: 'fixed',
            value: this.builderId
          
        }],
        width: 600,
        header: 'Add Project'
      },
      permission: { type: 'single', match: { key: 'CREATE_PROJECTS', value: true }}

    }
  }

}
