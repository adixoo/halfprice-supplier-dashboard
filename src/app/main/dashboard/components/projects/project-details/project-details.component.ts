import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanPermission } from 'src/@can/types/permission.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { UnitsComponent } from '../../units/units.component';
import { EditProjectComponent } from '../edit-project/edit-project.component';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  public detailViewData: CanDetailView;
  public tabViewConfig: CanTabView;
  public configurationPermission: CanPermission;

  @Input() projectId: any;
  dataSource : any
  constructor(private activedRoute: ActivatedRoute) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => this.projectId = parseInt(paramMap.get('id')));
  }
  ngOnInit() {
    this.configurationPermission = { type: 'single', match: { key: 'READ_PROJECTS', value: true }}
    
     // Detail View Config Init
     this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 3,
      // dataSource: this.dataSource,
      labelPosition: 'inline',
      action: [{
        action: {
          actionType: 'modal',
          modal: {
            component: EditProjectComponent,
            inputData: [
              {
                inputKey: 'projectId',
                type: 'key',
                key: 'id'
              },
              {
                inputKey: 'addressId',
                type: 'key',
                key: 'addressId'
              }
            ],
            // header: 'Edit Consumer',
            width: 600
          },
      permission: { type: 'single', match: { key: 'UPDATE_PROJECTS', value: true }}

        },
        icon: {
          name: 'edit',
          tooltip: 'Edit'
        },
      }],
      displayedFields: [
        {
          header: 'Project Name',
          type: 'text',
          value: 'name',
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        },
        {
          header :"Address",
          type : 'text',
          value:'address.address'
        },

        {
          header :"City",
          type : 'text',
          value:'address.city'
        },

        {
          header :"State",
          type : 'text',
          value:'address.state'
        }
      ],
      api: {
        apiPath: `/projects/${this.projectId}`,
        method: 'GET',
        params : new HttpParams().append('include', JSON.stringify([{ all: true }]))
      },
      header: "Project Details"
    }
  }

  onGetData(data) {
    this.tabViewConfig = {
      lazyLoading: true,
      tabs: []
    };
    this.tabViewConfig.tabs.push({
      component: UnitsComponent,
      label: 'Units',
      inputData: [{
        key: 'projectId',
        value: this.projectId
      }]
    })
   

  }
}
