// Custom Types
import { CanSidenav } from 'src/@can/types/navigation.type';
import { CanIconType } from 'src/@can/types/shared.type';

/**
 * To Show Sidemenu in UI
 * 
 * Also it can be protected with Roles and Permissions
 * 
 * Side Navigation Config
 */
 export const SideNavigationConfig: CanSidenav[] = [

    {
        type: 'single',
        url: '/dashboard/welcome',
        name: 'Home',
        icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    },
    {
        type: 'single',
        url: '/dashboard/orders',
        name: 'Orders',
        icon: { type: CanIconType.Flaticon, name: 'flaticon-box' }
    },
    // {
    //     type: 'single',
    //     url: '/dashboard/orders',
    //     name: 'Returns/RTO',
    //     icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    // },

    // {
    //     type: 'single',
    //      url: '/dashboard/orders',
    //     name: 'Inventory',
    //     icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    // },

    // {
    //     type: 'single',
    //      url: '/dashboard/orders',
    //     name: 'Payments',
    //     icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    // },

    {
        type: 'single',
         url: '/dashboard/catelouge-managment',
        name: 'Catelouge Management',
        icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    },
  {
        type: 'single',
        url: '/dashboard/products',
        name: 'Product',
        icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    },
    {
        type: 'single',
         url: '/dashboard/orders',
        name: 'Support',
        icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    },

    {
        type: 'single',
         url: '/dashboard/upload-images',
        name: 'Images Bulk Upload',
        icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    },
    {
        type: 'single',
        url: '/dashboard/orders',
        name: 'Settings',
        icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    },
    //  {
    //     type: 'single',
    //      url: '/dashboard/orders',
    //     name: 'Category',
    //     icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    
    // },
    // {
    //     type: 'group',
    //     name: 'Administration',
    //     icon: { type: CanIconType.Flaticon, name: 'flaticon-users' },
    //     group: [
    //         {
    //             type: 'single',
    //             url: '/dashboard/roles',
    //             name: 'Roles',
    //             icon: { type: CanIconType.Flaticon, name: 'flaticon-users' },
    //         },
    //         {
    //             type: 'single',
    //             url: '/dashboard/permissions',
    //             name: 'Permission',
    //             icon: { type: CanIconType.Flaticon, name: 'flaticon-settings' },
    //         }
    //     ]
    // },
  
   
    // {
    //     type: 'single',
    //     url: '/dashboard/faqs',
    //     name: `FAQ`,
    //     icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },
    //     permission: { type: 'single', match: { key: 'READ_FAQ', value: true } }

    // },
    // {
    //     type: 'single',
    //     url: '/dashboard/meta-info',
    //     name: `Meta Info`,
    //     icon: { type: CanIconType.Flaticon, name: 'flaticon-box' },

    // },
    // {
    //     type: 'group',
    //     name: 'Users',
    //     icon: { type: CanIconType.Flaticon, name: 'flaticon-users' },
    //     permission: { type: 'single', match: { key: 'READ_USER', value: true } },
    //     group: [
    //         {
    //             type: 'single',
    //             url: '/dashboard/customers',
    //             name: 'Customers',
    //             icon: { type: CanIconType.Flaticon, name: 'flaticon-users' },
    //         },
    
    //         {
    //             type: 'single',
    //             url: '/dashboard/internals',
    //             name: 'Internal Users',
    //             icon: { type: CanIconType.Flaticon, name: 'flaticon-users' },
    //         },
         
   
    //     ]
    // },          
    
 ];
//  src/app/main/dashboard/components/customers