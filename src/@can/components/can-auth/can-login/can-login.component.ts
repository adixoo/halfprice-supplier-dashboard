import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { CanNotificationService } from "src/@can/services/notification/notification.service";
import { CanAuth } from "src/@can/types/app-core.type";
import { CanAuthService } from "src/@can/services/auth/auth.service";
import { AppConfig } from "src/app/main/config/app.config";
import { CanAutoUnsubscribe } from "src/@can/decorators/auto-unsubscribe";
import { Subscription } from "rxjs";
import { CanPermissionService } from "src/@can/services/permission/permission.service";

@Component({
  selector: "can-login",
  templateUrl: "./can-login.component.html",
  styleUrls: ["./can-login.component.scss"],
})
// Cleared Subscriptions
@CanAutoUnsubscribe
export class CanLoginComponent implements OnInit {
  // Auth config
  private authConfig: CanAuth;

  // Logo url
  public logo: string;

  // Show otp form
  public generateOtpForm = true;

  // Show Login form
  public loginForm = false;

  // Login credentials
  public credentials = { mobile: "", otp: "" };

  // Disable buttons
  public disableGenerateOtpButton = false;
  public disableLoginButton = false;

  // Subscriptions
  private otpSubscription: Subscription;
  private loginSubscription: Subscription;

  constructor(
    private authService: CanAuthService,
    private permissionService: CanPermissionService,
    private notificationService: CanNotificationService,
    private router: Router
  ) {
    this.authConfig = AppConfig.authConfig;
    this.logo = this.authConfig.logoUrl;
  }

  ngOnInit() {}

  /**
   *
   * @param form :NgForm
   * Generate otp
   */
  public onGenerateOtp(form: NgForm): void {
    this.disableGenerateOtpButton = true;
    this.otpSubscription = this.authService.generateOtp(form.value).subscribe(
      (res) => {
        this.notificationService.showSuccess(res["message"]);
        this.generateOtpForm = false;
        this.loginForm = true;
        this.credentials.mobile = form.value.mobile;
        this.disableGenerateOtpButton = false;
      },
      (err) => {
        this.disableGenerateOtpButton = false;
        this.notificationService.showError(err.error.message);
      }
    );
  }

  /**
   * Login
   */
  // public onLogin(): void {
  //   this.disableLoginButton = true;
  //   this.loginSubscription = this.authService.login(this.credentials)
  //     .subscribe(
  //       res => {
  //         this.authService.setToken(res['token']);
  //         // this.permissionService.setPermissions();
  //         this.notificationService.showSuccess('Logged in successfully!');
  //         this.router.navigateByUrl(this.authConfig.successUrl);
  //         this.disableLoginButton = false;
  //       }, (err) => {
  //         this.disableLoginButton = false;
  //         this.notificationService.showError(err.error.error.message);
  //       });
  // }
  public onLogin(): void {
    // this.isLoading = true;
    this.disableLoginButton = true;
    this.loginSubscription = this.authService.login(this.credentials).subscribe(
      async (res) => {
        this.authService.setToken(res["token"]);
        this.authService.setUsers(res['token'])
        // await this.permissionService.setPermissions();
        this.notificationService.showSuccess("Logged in successfully!");
        // this.isLoading = false;
        this.router.navigateByUrl(this.authConfig.successUrl);
        this.disableLoginButton = false;
      },
      (err) => {
        this.disableLoginButton = false;
        this.notificationService.showError(err.error.error.message);
      }
    );
  }
}
